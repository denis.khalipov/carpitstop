Shader "Vertex Magic/Liquid" {
	Properties {
		_MainTex   ("Main", 2D) = "white" {}
		_MainTint  ("Tint", Color) = (1, 1, 1, 1)
		_Fill      ("Fill Amount", Float) = 0
		_WobbleX   ("WobbleX", Float) = 0
		_WobbleZ   ("WobbleZ", Float) = 0
		_TopColor  ("Top Color", Color) = (1, 1, 1, 1)
		_FoamColor ("Foam Line Color", Color) = (1, 1, 1, 1)
		_FoamWidth ("Foam Line Width", Float) = 0    
		_RimColor  ("Rim Color", Color) = (1, 1, 1, 1)
		_RimPower  ("Rim Power", Float) = 0
	}
	SubShader {
		Tags { "Queue" = "Transparent" }
		Pass {
			Zwrite On
			Cull Off 
			AlphaToMask On
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 view : TEXCOORD1;
				float3 norm : TEXCOORD2;
				float  edge : TEXCOORD3;
			};
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Fill, _WobbleX, _WobbleZ, _FoamWidth, _RimPower;
			fixed4 _TopColor, _RimColor, _FoamColor, _MainTint;

			float4 RotateAroundY (float3 v, float degree)
			{
				float a = degree * UNITY_PI / 180.0;
				float s, c;
				sincos(a, s, c);
				float2x2 m = float2x2(c, s, -s, c);
				return float4(v.yz , mul(m, v.xz)).xzyw;            
			}
			v2f vert (appdata_full v)
			{
				float3 wldpos = mul(unity_ObjectToWorld, v.vertex.xyz);
				float3 wldposX = RotateAroundY(wldpos, 360.0);
				float3 wldposZ = float3(wldposX.y, wldposX.z, wldposX.x);
				float3 wldposAdjusted = wldpos + (wldposX  * _WobbleX)+ (wldposZ * _WobbleZ);
			
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.edge = wldposAdjusted.y + _Fill;
				o.view = normalize(ObjSpaceViewDir(v.vertex));
				o.norm = v.normal;
				return o;
			}
			fixed4 frag (v2f i, fixed facing : VFACE) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _MainTint;

				// rim
				float3 V = normalize(i.view);
				float3 N = normalize(i.norm);
				float rim = 1.0 - pow(dot(N, V), _RimPower);
				float3 rimcol = _RimColor.rgb * smoothstep(0.5, 1.0, rim);

				// foam edge
				float4 foam = step(i.edge, 0.5) - step(i.edge, 0.5 - _FoamWidth);
				float4 result = step(i.edge, 0.5) - foam;
				float4 c1 = result * col;
				float4 c = c1 + (foam * _FoamColor);               
				c.rgb += rimcol;
				
				float4 topcol = _TopColor * (foam + result);
				return facing > 0 ? c: topcol;
			}
			ENDCG
        }
    }
	FallBack "Diffuse"
}