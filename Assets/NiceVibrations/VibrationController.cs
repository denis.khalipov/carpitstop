﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class VibrationController : Singleton<VibrationController>
{
    private void Start()
    {

      //  DontDestroyOnLoad(this);
#if UNITY_IOS
        MMVibrationManager.iOSInitializeHaptics();
#endif
    }

    protected virtual void OnDisable()
    {
#if UNITY_IOS
        MMVibrationManager.iOSReleaseHaptics();
#endif
    }

    protected virtual void TriggerDefault()
    {
#if UNITY_IOS || UNITY_ANDROID
        Handheld.Vibrate();
#endif
    }
    public void VibrateWithTypeSelection()
    {
        MMVibrationManager.Haptic(HapticTypes.Selection);
    }
    public void VibrateWithTypeMEDIUMIMPACT()
    {
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }
    public void VibrateWithTypeLIGHTIMPACT()
    {
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }
    public void VibrateWithTypeHAIDIMPACT()
    {
        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }
    public void VibrateWithType(HapticTypes _type)
    {
            MMVibrationManager.Haptic(_type);
    }
}
