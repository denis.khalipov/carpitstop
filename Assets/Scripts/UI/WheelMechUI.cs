﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WheelMechUI : Singleton<WheelMechUI>
{
    public event Action<int> Action_ItemSelected;

    [SerializeField] private Button[] m_SelectButtons;
    [SerializeField] private Sprite[] m_PreviewWheelIcons;
    [SerializeField] private Image m_NeededIcon;

    private CanvasGroup m_CG;
    private int[] m_RandomWheels = new int[3];

    private void Start()
    {
        m_CG = GetComponent<CanvasGroup>();
        m_CG.blocksRaycasts = false;
        SetRandomWheel();
    }

    private void SetRandomWheel()
    {
        for (int i = 0; i < 3; i++)
        {
            m_RandomWheels[i] = Random.Range(0, m_PreviewWheelIcons.Length);
        }

        for (int i = 0; i < 3; i++)
        {
            m_SelectButtons[i].image.sprite = m_PreviewWheelIcons[m_RandomWheels[i]];
        }
        m_NeededIcon.sprite = m_PreviewWheelIcons[m_RandomWheels[Random.Range(0, m_RandomWheels.Length)]];
    }

    private void MoveToNextStep(int _index)
    {
        m_CG.blocksRaycasts = false;
        m_CG.DOFade(0, 0.5f).OnComplete(() =>
        {
            Action_ItemSelected?.Invoke(m_RandomWheels[_index]);
        });
    }

    public void SelectButtonClick(int _index)
    {
        MoveToNextStep(_index);
    }

    public void ShowMenu()
    {
        m_CG.blocksRaycasts = false;
        m_CG.DOFade(1, 0.5f).OnComplete(() => { m_CG.blocksRaycasts = true; });        
    }
}
