﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;
using DG.Tweening;
using System.Linq;

public class ClearBrakeUI : Singleton<ClearBrakeUI>
{
    public event Action Action_BrakeSelected;
    public event Action Action_ClearClosed;
  
    [SerializeField] private RectTransform m_SelectMaterialContainer, m_SetBrakePosContainer, m_ClearProgressContainer;
    [SerializeField] private Image m_NeededIcon, m_SetIndicator, m_ClearProgress;
    [SerializeField] private Sprite[] m_PreviewBrakeIcons, m_NeededBrakeIcons;
    [SerializeField] private Material[] m_Materials;
    [SerializeField] private Button[] m_SelectButtons;
    [SerializeField] private Button SetPositionButton, m_DoneButton;
    private CanvasGroup m_CG;    
    private Sequence m_IndicatorSequence;
    private GameObject m_BreakObject;
    
    public GameObject SetBrakeObject
    {
        set
        {
            m_BreakObject = value;
        }
    }

    public float SetClearProgress
    {
        set
        {
            m_ClearProgress.fillAmount = value;
            if (value > 0.575f)
            {
                m_DoneButton.interactable = true;
            }
        }
    }

    private void Start()
    {        
        m_CG = GetComponent<CanvasGroup>();        
        SetRandomBrake();
        InitButtonListeners();
    }

    private void SetRandomBrake()
    {
        for (int i = 0; i < m_SelectButtons.Length; i++)
        {
            m_SelectButtons[i].image.sprite = m_PreviewBrakeIcons[i];
        }
        m_NeededIcon.sprite = m_NeededBrakeIcons[Random.Range(0, m_PreviewBrakeIcons.Length)];
    }

    private void InitButtonListeners()
    {
        m_DoneButton.onClick.AddListener(DoneButtonClick);
        SetPositionButton.onClick.AddListener(SetPosButtonClick);
    }

    private void BrakeSelected(int _index)
    {
        m_CG.blocksRaycasts = false;
        m_CG.DOFade(0, 0.5f).OnComplete(() =>
        {
            Material[] mats = m_BreakObject.GetComponent<MeshRenderer>().materials;
            mats[2] = m_Materials[_index];
            m_BreakObject.GetComponent<MeshRenderer>().materials = mats;
            SelectPosition();
        });        
    }

    private void DoneButtonClick()
    {
        m_CG.blocksRaycasts = false;
        m_CG.DOFade(0, 0.5f).OnComplete(()=> { Action_ClearClosed?.Invoke(); });        
    }

    private void SelectPosition()
    {
        SetUIState(null);
        m_CG.blocksRaycasts = true;
        m_BreakObject.SetActive(true);
        m_CG.DOFade(1, 0.5f);
        m_IndicatorSequence = DOTween.Sequence()
            .Append(m_BreakObject.transform.DOLocalRotate(new Vector3(-180, 0, 0), 2f, RotateMode.LocalAxisAdd).SetEase(Ease.Linear)).SetLoops(-1, LoopType.Yoyo)
            .Join(m_SetIndicator.rectTransform.DOAnchorPosX(-m_SetIndicator.rectTransform.anchoredPosition.x, 2f).SetEase(Ease.Linear)).SetLoops(-1, LoopType.Yoyo);
    }

    private void SetPosButtonClick()
    {
        m_IndicatorSequence.Kill();
        m_CG.blocksRaycasts = false;
        m_CG.DOFade(0, 0.5f).OnComplete(() =>
        {
            SetUIState(false);
            m_CG.blocksRaycasts = true;
            Action_BrakeSelected?.Invoke();
        });
    }

    private void SetUIState(bool? _isClearProgress)
    {
        m_ClearProgressContainer.gameObject.SetActive(_isClearProgress == true);
        m_SelectMaterialContainer.gameObject.SetActive(_isClearProgress == false);
        m_SetBrakePosContainer.gameObject.SetActive(_isClearProgress == null);        
    }

    public void SelectButtonClick(int _index)
    {
        BrakeSelected(_index);
    }

    public void ShowMenu(bool _isClearMenu, bool _isFadeZero = false)
    {        
        m_CG.blocksRaycasts = false;
        if (_isFadeZero)
        {
            Sequence sequence = DOTween.Sequence()
            .Append(m_CG.DOFade(0, 0.5f))
            .AppendInterval(0.5f).OnComplete(() =>
            {
                SetUIState(_isClearMenu);
                m_CG.DOFade(1, 0.5f).OnComplete(() => { m_CG.blocksRaycasts = true; });
            });
        }
        else
        {
            SetUIState(_isClearMenu);
            m_CG.DOFade(1, 0.5f).OnComplete(() => { m_CG.blocksRaycasts = true; });
        }
    }
}
