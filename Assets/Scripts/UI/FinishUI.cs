﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class FinishUI : Singleton<FinishUI>
{

    [SerializeField] private Text[] AllText;
    [SerializeField] private Image[] AllImageMech;
    [SerializeField] private Text TotalText;
    [SerializeField] private GameObject[] OpenMechActive;
    [SerializeField] private Sprite[] MechSprite;
    [SerializeField] private Text NumberRepairText;
    [SerializeField] private Image[] Galki;
    private int[] CurrenMech;
    private void Start()
    {
       
    }
    public void InfoFinish(int[] Mech)
    {
        CurrenMech = Mech;
        NumberRepairText.text = "REPAIR " + (PlayerPrefs.GetInt("Level", 0) + 1).ToString() + " COMPLETE";
        int goldRepari = StartUIController.Instance.GetRepairGold;
        TotalText.text = goldRepari.ToString();

        StartCoroutine(AnimGalki());
        for (int i = 0; i < OpenMechActive.Length; i++)
        {
            OpenMechActive[i].SetActive(false);
        }
        
        for (int i = 0; i < Mech.Length; i++)
        {
            
            AllImageMech[i].sprite = MechSprite[Mech[i]];
            OpenMechActive[i].SetActive(true);
            if (i+1 != Mech.Length)
            {
                int ps = goldRepari / Mech.Length;
                int prise =  (int) Random.Range(ps * 0.8f, ps*1.2f);
                AllText[i].text = prise.ToString();
                goldRepari -= prise;
            }
            else
            {
                AllText[i].text = goldRepari.ToString();
            }


            //goldRepari -= Random.Range(20, goldRepari/2);
            //AllText[i].text = goldRepari.ToString();
        }

        
    }
    private IEnumerator AnimGalki()
    {
        for (int i = 0; i < CurrenMech.Length; i++)
        {
            Galki[i].transform.localScale = Vector3.zero;
        }
        for (int i = 0; i < CurrenMech.Length; i++)
        {
          
            Galki[i].transform.DOScale(1, 0.5f);
            yield return new WaitForSeconds(0.7f);
        }
    }
    void Update()
    {
        
    }
}
