﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class GamePlayMenu : Singleton<GamePlayMenu>
{
    [SerializeField] private RectTransform m_StepsContainer, m_LevelsContainer;
    [SerializeField] private CanvasGroup[] m_BotProgressCGandDownButton;
    [SerializeField] private GameObject m_StepPrefab, m_LevelPrefab;
    [SerializeField] private Text m_LevelText;
    [SerializeField] private Sprite[] m_NewMechanicIcons;

    private LevelStepItem[]
        m_LevelSteps,
        m_Levels;
    private int m_CurrentLevel, m_MechCount;
    private int m_CurrentStep = 0;
    
    private void Start()
    {
        m_CurrentLevel = PlayerPrefs.GetInt("Level", 0);       
    }

    private void OnEnable()
    {
        GameController.Instance.Action_StepComplete += NextLevelStep;
        GameController.Instance.Action_LevelComplete += LevelComplete;
        CreateUI();
    }

    private void OnDisable()
    {
        GameController.Instance.Action_StepComplete -= NextLevelStep;
        GameController.Instance.Action_LevelComplete -= LevelComplete;
    }

    private void CreateUI()
    {
        if (m_LevelSteps != null)
            return;
        m_LevelSteps = new LevelStepItem[3];
        m_Levels = new LevelStepItem[9];


        for (int i = 0; i < 3; i++)
        {
            m_LevelSteps[i] = Instantiate(m_StepPrefab, m_StepsContainer).GetComponent<LevelStepItem>();
        }

        for (int i = 0; i < 9; i++)
        {
            LevelStepItem levelItem = Instantiate(m_LevelPrefab, m_LevelsContainer).GetComponent<LevelStepItem>();
            m_Levels[i] = levelItem;
        }
    }

    public void SetUiData(int _mechCount)
    {
        m_LevelText.text = "REPAIR " + (m_CurrentLevel + 1).ToString();
        m_MechCount = _mechCount;

        for (int i = 0; i < m_LevelSteps.Length; i++)
        {
            m_LevelSteps[i].gameObject.SetActive(i < _mechCount);
            m_LevelSteps[i].DisableImages();
        }
        UpdateUiItems();
    }

    private void UpdateUiItems()
    {
        for (int i = 0; i < m_MechCount; i++)
        {
            if (i == m_CurrentStep)
                m_LevelSteps[i].CurrentStep();
            if (i < m_CurrentStep)
                m_LevelSteps[i].StepComplete();
            if (i > m_CurrentStep)
                m_LevelSteps[i].DisableImages();
        }
        int convertedIndex = m_CurrentLevel - (int)(m_CurrentLevel / 9 * 9);
        int startMechIndex = ((m_CurrentLevel / 9) * 9) + 1;
        List<int> mechLevels = LevelController.Instance.GetOpenNewMech.ToList();
        List<int> mechSeq = LevelController.Instance.GetAllMechanic.ToList();
        mechSeq.RemoveAt(0);

        for (int i = 0; i < 9; i++)
        {
            if (i == convertedIndex)
            {
                m_Levels[i].CurrentStep();
                m_Levels[i].CurrentAnimation();
            }
            if (i < convertedIndex)
                m_Levels[i].StepComplete();
            if (i > convertedIndex)
                m_Levels[i].DisableImages();

            int mechLevel = mechLevels.FirstOrDefault(index => index == startMechIndex + i);
            int mechIndex = mechLevels.IndexOf(mechLevel);
            Sprite sprite = mechIndex == -1 ? null : m_NewMechanicIcons[mechSeq[mechIndex]];
            m_Levels[i].SetMechIcon(sprite);
        }
    }

    private void NextLevelStep()
    {
        m_CurrentStep++;
        UpdateUiItems();
    }

    private void LevelComplete()
    {
        m_CurrentLevel++;
        m_CurrentStep = 0;
        //SetUiData();
        UpdateUiItems();
        for (int i = 0; i < m_BotProgressCGandDownButton.Length; i++)
        {
            m_BotProgressCGandDownButton[i].DOFade(1, 0.25f);
            m_BotProgressCGandDownButton[i].blocksRaycasts = true;
        }
     
    }

    public void GameStart()
    {
        for (int i = 0; i < m_BotProgressCGandDownButton.Length; i++)
        {
            m_BotProgressCGandDownButton[i].DOFade(0, 0.25f);
            m_BotProgressCGandDownButton[i].blocksRaycasts = false;
        }
    }
}
