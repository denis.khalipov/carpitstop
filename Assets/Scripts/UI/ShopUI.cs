﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class ShopUI : MonoBehaviour
{
    private CanvasGroup ShopContainer;
    [SerializeField] private Button  ShopButton,BackButton;
    [SerializeField] private Image Fade;
    [SerializeField] private GameObject Sun;
    int fadeIndex = 0;
    void Start()
    {
        ShopButton.onClick.AddListener(ActiveShop);
        BackButton.onClick.AddListener(ActiveShop);
        ShopContainer = GetComponent<CanvasGroup>();
    }

    private void ActiveShop()
    {
        if(fadeIndex == 1)
        {
            fadeIndex = 0;
            ShopContainer.blocksRaycasts = false;
        }
        else
        {
            fadeIndex = 1;
            ShopContainer.blocksRaycasts = true;
        }

        ShopContainer.DOFade(fadeIndex, 0.2f);

    }
    void Update()
    {
        Sun.transform.Rotate(0, 0, 80 * Time.deltaTime);
    }
}
