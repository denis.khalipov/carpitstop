﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelStepItem : MonoBehaviour
{
    [SerializeField] private Image m_CurrentImage, m_CompleteImage, m_MechIcon;
    

    public void StepComplete()
    {
        m_CompleteImage.gameObject.SetActive(true);
        m_CurrentImage.gameObject.SetActive(false);
    }

    public void CurrentStep()
    {
        m_CurrentImage.gameObject.SetActive(true);
    }

    public void DisableImages()
    {
        m_CompleteImage.gameObject.SetActive(false);
        m_CurrentImage.gameObject.SetActive(false);
    }

    public void CurrentAnimation()
    {
        m_CurrentImage.transform.DOScale(1.1f, 0.5f).SetEase(Ease.InCirc).SetLoops(-1, LoopType.Yoyo);
    }

    public void SetMechIcon(Sprite _mechIcon = null)
    {
        if (_mechIcon)
        {
            m_MechIcon.sprite = _mechIcon;            
        }
        m_MechIcon.gameObject.SetActive(_mechIcon == null ? false : true);
    }
}
