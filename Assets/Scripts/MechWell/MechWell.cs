﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MechWell : Singleton<MechWell>
{
    [SerializeField] private GameObject Pictolet;
    [SerializeField] private Camera CurrenCamera;
    [SerializeField] private GameObject[] Wells;
    [SerializeField] private MeshRenderer BackGroundCar;
    [SerializeField] private GameObject StartPosPictol;
    [SerializeField] private Image Tutorial;
    [SerializeField] private bool m_IsClearBrake = false;
    private Vector3 mOffset;
    private float mZCoord;
    private bool WellMechActive = false;
    private Vector3 offset;
    private GameObject CurrenCar;
    private Vector3 m_CameraStartPos, m_PreviewPosition;

    void Start()
    {
        ActiveWell();
        Pictolet.transform.position = StartPosPictol.transform.position;
        m_CameraStartPos = CurrenCamera.transform.position;
        //Tutorial.DOFade(1, 0.5f).OnComplete(() =>
        //{
        //    Tutorial.DOFade(1, 0.5f).SetLoops(-1, LoopType.Yoyo);
        //});
        int i = PlayerPrefs.GetInt("Tutor_3", 0);
        if (i == 0)
        {
            Tutorial.gameObject.SetActive(true);
            PlayerPrefs.SetInt("Tutor_3", 1);
        }

        if (m_IsClearBrake)
        {
            Pictolet.GetComponent<Pistol>().DiactivatePistol();
        }
    }

    public void ActiveWell()
    {
        CurrenCar = CarScript.Instance.gameObject;
        CarScript car = CurrenCar.GetComponent<CarScript>();
        Wells[car.GetNumberWell].SetActive(true);

        Material[] mats = BackGroundCar.GetComponent<MeshRenderer>().materials;
        mats[2] = car.GetMaterialCar;
        BackGroundCar.GetComponent<MeshRenderer>().materials = mats;      
    }

    private void Update()
    {
        PictolController();
    }

    private void PictolController()
    {
        if (!Pictolet.GetComponent<Pistol>().IsPistolActive || m_IsClearBrake)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            if (!WellMechActive)
            {
                WellMechActive = true;
                Tutorial.gameObject.SetActive(false);
            }
            mZCoord = Camera.main.WorldToScreenPoint(Pictolet.transform.position).z;
            mOffset = Pictolet.transform.position - GetMouseAsWorldPoint();
            m_PreviewPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(0) && WellMechActive)
        {
            //позиция  за мышкой
            CameraMove();
            Pictolet.transform.position = GetMouseAsWorldPoint() + mOffset;
        }

    }

    private void CameraMove()
    {
        Vector3 cameraStep = (Input.mousePosition - m_PreviewPosition).normalized / 2f;
        CurrenCamera.transform.localPosition = Vector3.Lerp(CurrenCamera.transform.localPosition, CurrenCamera.transform.localPosition + cameraStep / 5f, Time.deltaTime);
        m_PreviewPosition = Input.mousePosition;
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        // Convert it to world point
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }
}
