﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Pistol : MonoBehaviour
{
    [SerializeField] private GameObject StartRay;
    [SerializeField] private ParticleSystem Effect;
    [SerializeField] private LayerMask MaskBolt;

    
    private bool m_IsActive = true;
    private Vector3 m_StartPos;

    public bool IsPistolActive => m_IsActive;

    void Start()
    {
        m_StartPos = transform.localPosition;
    }

    public void DiactivatePistol()
    {
        m_IsActive = false;
        gameObject.SetActive(false);
    }

    public void HidePistol()
    {
        m_IsActive = false;
        transform.DOLocalMove(m_StartPos + Vector3.left * 3, 2f);
    }

    public void ShowPistol()
    {
        transform.DOLocalMove(m_StartPos, 2f).OnComplete(()=>
        {
            m_IsActive = true;
        });
    }
    // Update is called once per frame
    void Update()
    {
        if (!m_IsActive)
            return;

        RaycastHit hit;
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, 5.45f, 5.9f), Mathf.Clamp(transform.localPosition.y, -3, -2.65f), 4.992f);
        if (Physics.Raycast(StartRay.transform.position, StartRay.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, MaskBolt) )
        {
            Debug.DrawRay(StartRay.transform.position, StartRay.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

            transform.DOShakeRotation(0.1f, new Vector3(0.5f, 0, 0), 1, 0.1f);
            hit.collider.gameObject.GetComponent<Bolt>().RotateBolt();
            VibrationController.Instance.VibrateWithTypeSelection();
            if (Effect.isStopped)
            {
                Effect.Play();
            }
        }
        else
        {
            transform.localEulerAngles = new Vector3(0, 90, 0);
            Debug.DrawRay(StartRay.transform.position, StartRay.transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            
            if (Effect.isPlaying)
            {
                Effect.Stop();
            }
        }
    }
}
