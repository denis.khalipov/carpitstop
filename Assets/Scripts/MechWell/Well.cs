﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Well : Singleton<Well>
{
    private List<GameObject> bolts = new List<GameObject>();
    [SerializeField] private GameObject m_PistolObject;
    [SerializeField] private GameObject CurrenWell;
    [SerializeField] private GameObject[] m_UpgradedWheels;
    [SerializeField] private GameObject ObvodkaWell;
    [SerializeField] private GameObject[] BoltEffect;
    [SerializeField] private GameObject Disk;
    [SerializeField] private Material NewMaterilaDisk;
    [SerializeField] private ParticleSystem[] EffectWellMove;

    private int CountBolt;
    private Vector3 StartPos;
    private GameObject m_UpgradedWheel;

    public GameObject[] GetEffectBolt
    {
        get { return BoltEffect; }
    }

    void Start()
    {
        ObvodkaWell.GetComponent<MeshRenderer>().material = CarScript.Instance.GetMaterialWell;
        StartPos = CurrenWell.transform.localPosition;
      
       // CurrenWell.GetComponent<MeshRenderer>().materials[2] = CarScript.Instance.GetMaterialWell;
    }

    private void OnEnable()
    {
        WheelMechUI.Instance.Action_ItemSelected += CompleteWheelMech;
    }

    private void OnDisable()
    {
        WheelMechUI.Instance.Action_ItemSelected -= CompleteWheelMech;
    }

    public GameObject SetBolt
    {
        set { bolts.Add(value); }
    }

    public void ChecOffBolt() //чекает на выкрученные болты
    {
        CountBolt++;
        if (bolts.Count == CountBolt)
        {
            MoveWheelOut(false);
        }
    }

    private void CompleteWheelMech(int _wheelIndex)
    {
        m_UpgradedWheel = m_UpgradedWheels[_wheelIndex];
        var newWheel = Instantiate(m_UpgradedWheels[_wheelIndex], CurrenWell.transform.position, CurrenWell.transform.rotation, CurrenWell.transform.parent);
        CurrenWell = newWheel;
        MoveWheelBack();
    }

    public void ChecOnBolt() //чекает на вкрученые болты
    {
        CountBolt++;
        if (bolts.Count == CountBolt)
        {
            Win(); 
        }
    }

    private void Win()
    {
        if (m_UpgradedWheel)
            CarScript.Instance.SetUpgradedWheel(m_UpgradedWheel);
        CarScript.Instance.WinNextMech();
    }

    private void MoveWheelOut(bool _isRollBack)
    {
        Sequence sequence = DOTween.Sequence()
            .Append(CurrenWell.transform.DOLocalMoveX(CurrenWell.transform.localPosition.x - 0.001f, 0.3f))
            .Append(CurrenWell.transform.DOLocalMoveY(CurrenWell.transform.localPosition.y - 0.02f, 0.5f))
            .OnComplete(() => 
            {
                CountBolt = 0;
                if (_isRollBack)
                    MoveWheelBack();
                else
                {
                    WheelMechUI.Instance.ShowMenu();
                }
            });
    }

    private void MoveWheelBack()
    {
        Disk.GetComponent<MeshRenderer>().material = NewMaterilaDisk;
        Sequence sequence = DOTween.Sequence()
            .Append(CurrenWell.transform.DOLocalMoveY(StartPos.y, 0.5f))
            .Append(CurrenWell.transform.DOLocalMoveX(StartPos.x, 0.3f))
            .AppendInterval(0.7f)
            .OnComplete(() =>
            {
                for (int i = 0; i < bolts.Count; i++)
                {
                    bolts[i].GetComponent<Bolt>().BackMove(0.1f * i);
                }
            });
    }
}
