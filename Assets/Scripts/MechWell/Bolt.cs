﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Bolt : MonoBehaviour
{
    private float Timer = 1.5f;
    private BoxCollider coll;
    [SerializeField] private Material EndMaterial, StartMaterial;
    [SerializeField] private Material m_DefaultOutline, m_RotateOutline;
    
   // private Material StartMaterial;
     [SerializeField] private GameObject PosEnd;
    private Color32 colordd;
    private Vector3 StartPos;
    private bool Revers;
    private MeshRenderer m_StrokeEffect;

    private Color m_RotateColor, m_WaitColor;

    private float m_WaitColorTimer = 0.15f;

    public float GetTime
    {
        get {return Timer; }
    }

    void Start()
    {
      //  StartMaterial = GetComponent<MeshRenderer>().material;
        coll = GetComponent<BoxCollider>();
        Well.Instance.SetBolt = gameObject;
        var effect = Instantiate(Well.Instance.GetEffectBolt[1], transform);
        m_StrokeEffect = effect.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        m_WaitColorTimer -= Time.deltaTime;
        if (m_WaitColorTimer <= 0)
        {
            m_StrokeEffect.material = m_DefaultOutline;
        }
    }

    public void BackMove(float _interval)
    {
        Sequence sequence = DOTween.Sequence()
            .AppendInterval(_interval)
            .Append(transform.DOMove(StartPos, 0.5f)).OnComplete(()=> { m_StrokeEffect.gameObject.SetActive(true); });
    }

    public void RotateBolt()
    {
        m_WaitColorTimer = 0.15f;
        if (!Revers)
        {
            if (Timer > 0.1f)
            {
                m_StrokeEffect.material = m_RotateOutline;
                transform.Rotate(300*3f*Time.deltaTime, 0, 0);
                transform.Translate(-0.02f * Time.deltaTime, 0, 0);
                Timer -= Time.deltaTime * 2f;
            }
            else
            {

                VibrationController.Instance.VibrateWithTypeMEDIUMIMPACT();
                m_StrokeEffect.gameObject.SetActive(false);
                coll.enabled = false;
                StartPos = transform.position;
                transform.DOMove(PosEnd.transform.position, 1f).OnComplete(() =>
                {
                    Revers = true;
                    GetComponentInParent<Well>().ChecOffBolt();
                    coll.enabled = true;
                    GetComponent<MeshRenderer>().material = EndMaterial;
                });
                Timer = 1.5f;
            }
        }
        else
        {
            if (Timer > 0.1f)
            {
                m_StrokeEffect.material = m_RotateOutline;
                transform.Rotate(300 * 2.5f * Time.deltaTime, 0, 0);
                transform.Translate(+0.01f * Time.deltaTime, 0, 0);
                Timer -= Time.deltaTime *2;

                GetComponent<MeshRenderer>().material.color = Color.Lerp(GetComponent<MeshRenderer>().material.color, StartMaterial.color, Time.deltaTime*4);
            }
            else
            {
                m_StrokeEffect.gameObject.SetActive(false);
                Debug.Log("Закрутил");
                coll.enabled = false;

                VibrationController.Instance.VibrateWithTypeSelection();
                GetComponent<MeshRenderer>().material = StartMaterial;
                GetComponentInParent<Well>().ChecOnBolt();
                Instantiate(Well.Instance.GetEffectBolt[0], transform);
            }
        }
    }
}
