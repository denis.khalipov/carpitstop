﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BrakeCleaner : MonoBehaviour
{
    [SerializeField] private LayerMask m_CorrosionLayer;
    [SerializeField] private Transform m_RotationDisk, m_Bolgarka;
    [SerializeField] private ParticleSystem m_ClearEffect;

    private bool m_IsActive = false;
    private Vector3 mOffset;
    private float mZCoord;

    private Vector3 m_StartPos;

    private void Start()
    {
        m_StartPos = transform.localPosition;
        transform.localPosition = m_StartPos - transform.forward * 2;
    }

    public void ShowObject()
    {
        transform.DOLocalMove(m_StartPos, 1f)
            .OnComplete(() => 
            {
                m_IsActive = true;
            });        
    }

    public void HideObject()
    {
        m_ClearEffect.Stop();
        m_IsActive = false;
        transform.DOLocalMove(m_StartPos - transform.forward * 2, 1f);
    }

    private void Update()
    {
        m_RotationDisk.Rotate(Vector3.back * 15f, Space.Self);
        if (m_IsActive)
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, 0.035f, transform.forward, out hit, 10))
            {
                CorrosionPart corPart = hit.transform.GetComponent<CorrosionPart>();
                corPart.Activate();
            }
            
            InputControll();
        }
    }

    private void InputControll()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mZCoord = Camera.main.WorldToScreenPoint(transform.position).z;
            mOffset = transform.position - GetMouseAsWorldPoint();
        }

        if (Input.GetMouseButton(0))
        {
            m_Bolgarka.localPosition = Vector3.Lerp(m_Bolgarka.localPosition, new Vector3(-0.2f, -0.172f, 0.25f), Time.deltaTime * 5f);
            m_Bolgarka.localRotation = Quaternion.Lerp(m_Bolgarka.localRotation, new Quaternion(60f, -180f, -60f, 1f), Time.deltaTime * 0.1f);
            if (mZCoord == 0f)
            {
                mZCoord = Camera.main.WorldToScreenPoint(transform.position).z;
                mOffset = transform.position - GetMouseAsWorldPoint();
            }
            //позиция  за мышкой
            transform.position = GetMouseAsWorldPoint() + mOffset;
            if (!m_ClearEffect.isPlaying)
                m_ClearEffect.Play();
        }
        else
        {
            m_ClearEffect.Stop();
            m_Bolgarka.localRotation = Quaternion.Lerp(m_Bolgarka.localRotation, new Quaternion(0f, -180f, 0f, 1f), Time.deltaTime * 0.5f);
            m_Bolgarka.localPosition = Vector3.Lerp(m_Bolgarka.localPosition, new Vector3(0f, -0.172f, -0.5f), Time.deltaTime * 5f);
        }
    }



    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        // Convert it to world point
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
    }
}
