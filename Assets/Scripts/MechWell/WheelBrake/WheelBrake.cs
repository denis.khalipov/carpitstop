﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelBrake : Singleton<WheelBrake>
{
    [SerializeField] private GameObject CurrenWell;
    [SerializeField] private GameObject Disk;
    [SerializeField] private Material NewMaterilaDisk;
    [SerializeField] private ClearBrakeMech m_ClearBrakeMech;

    private Vector3 StartPos;

    void Start()
    {
        StartPos = CurrenWell.transform.localPosition;
        MoveWheelOut();
    }

    private void MoveWheelOut()
    {
        Sequence sequence = DOTween.Sequence()
            .AppendInterval(1f)
            .Append(CurrenWell.transform.DOLocalMoveX(CurrenWell.transform.localPosition.x - 0.001f, 0.3f))
            .Append(CurrenWell.transform.DOLocalMoveY(CurrenWell.transform.localPosition.y - 0.02f, 0.5f))
            .OnComplete(() =>
            {
                m_ClearBrakeMech.Activate();
                m_ClearBrakeMech.Action_ClearEnd += MoveWheelBack;
            });
    }

    private void MoveWheelBack()
    {
        Disk.GetComponent<MeshRenderer>().material = NewMaterilaDisk;
        Sequence sequence = DOTween.Sequence()
            .Append(CurrenWell.transform.DOLocalMoveY(StartPos.y, 0.5f))
            .Append(CurrenWell.transform.DOLocalMoveX(StartPos.x, 0.3f))
            .AppendInterval(0.7f)
            .OnComplete(() =>
            {
                Win();
            });
    }

    private void Win()
    {
        CarScript.Instance.WinNextMech();
    }
}
