﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CorrosionSetUpper : MonoBehaviour
{
    public event Action Action_RemovePart;

    [SerializeField] private CorrosionPart[] m_CorrosionParts;

    public int GetCorCount => m_CorrosionParts.Length;

    public void RemovePart()
    {
        Action_RemovePart?.Invoke();
    }

    public void DisableAllParts()
    {
        for (int i = 0; i < m_CorrosionParts.Length; i++)
        {
            if (m_CorrosionParts[i])
                m_CorrosionParts[i].gameObject.SetActive(false);
        }
    }
}
