﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using Random = UnityEngine.Random;

public class ClearBrakeMech : MonoBehaviour
{
    public event Action Action_ClearEnd;
    [SerializeField] private Transform m_CameraTransofrm, m_CameraPoint;
    [SerializeField] private GameObject m_BreakObject, m_DiskObject;
    [SerializeField] private Transform[] m_CorrosionPrefabs;
    [SerializeField] private BrakeCleaner m_Cleaner;

    private int m_PartsCount;
    private int m_ClearedParts;
    private Vector3 m_CamDefaultPosition;
    private Quaternion m_CamDefaultRotation;
    private Sequence m_BrakeSequence;
    private CorrosionSetUpper m_CorrosionSetUpper;
    private int m_PartToVibration = 4;

    private void Awake()
    {
        CreateCorrision();
    }

    private void Start()
    {
        m_CamDefaultPosition = m_CameraTransofrm.position;
        m_CamDefaultRotation = m_CameraTransofrm.rotation;        
        m_BreakObject.SetActive(false);
    }

    private void CreateCorrision()
    {
        var corObj = Instantiate(m_CorrosionPrefabs[Random.Range(0, m_CorrosionPrefabs.Length)], transform);
        m_CorrosionSetUpper = corObj.GetComponent<CorrosionSetUpper>();
        m_PartsCount = m_CorrosionSetUpper.GetCorCount;
    }

    private void OnEnable()
    {
        ClearBrakeUI.Instance.Action_ClearClosed += ClearMechClosed;
        m_CorrosionSetUpper.Action_RemovePart += RemovePart;
    }

    private void OnDisable()
    {
        ClearBrakeUI.Instance.Action_ClearClosed -= ClearMechClosed;
        m_CorrosionSetUpper.Action_RemovePart -= RemovePart;
        ClearBrakeUI.Instance.Action_BrakeSelected -= BrakeSelected;
    }

    private void RemovePart()
    {
        m_ClearedParts++;
        m_PartToVibration--;
        if (m_PartToVibration <= 0)
        {
            m_PartToVibration = 4;
            VibrationController.Instance.VibrateWithTypeLIGHTIMPACT();
        }
        if (m_ClearedParts >= m_PartsCount * 0.99f)
        {
            m_CorrosionSetUpper.DisableAllParts();
            ClearMechClosed();
        }
        else
        {
            ClearBrakeUI.Instance.SetClearProgress = (float)m_ClearedParts / m_PartsCount;
        }
    }

    public void Activate()
    {
        Sequence sequence = DOTween.Sequence()
            .Append(m_CameraTransofrm.DOMove(m_CameraPoint.position, 1f))
            .Join(m_CameraTransofrm.DORotateQuaternion(m_CameraPoint.rotation, 1f))
            .OnComplete(()=> { m_Cleaner.ShowObject(); ClearBrakeUI.Instance.ShowMenu(true); });        
    }

    private void BrakeSelected()
    {
        ClearBrakeUI.Instance.Action_BrakeSelected -= BrakeSelected;
        Sequence sequence = DOTween.Sequence()
            .Append(m_CameraTransofrm.DOMove(m_CamDefaultPosition, 1f))
            .Join(m_CameraTransofrm.DORotateQuaternion(m_CamDefaultRotation, 1f))
            .OnComplete(() => { Action_ClearEnd?.Invoke(); });              
    }

    private void ClearMechClosed()
    {
        m_Cleaner.HideObject();
        ClearBrakeUI.Instance.Action_BrakeSelected += BrakeSelected;
        ClearBrakeUI.Instance.SetBrakeObject = m_BreakObject;
        ClearBrakeUI.Instance.ShowMenu(false, true);
    }
}
