﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrosionPart : MonoBehaviour
{
    [SerializeField] private CorrosionSetUpper m_BrakeScript;

    private Rigidbody m_RB;
    private BoxCollider m_Collider;
    private bool m_IsDisabled = false;

    private void Start()
    {
        m_RB = GetComponent<Rigidbody>();
        m_Collider = GetComponent<BoxCollider>();
    }

    public void Activate()
    {
        if (m_IsDisabled)
            return;
        m_IsDisabled = true;
        m_Collider.enabled = false;
        m_RB.useGravity = true;
        m_RB.isKinematic = false;
        m_RB.AddForce(new Vector3(Random.Range(0, 2f), 1f, -1f), ForceMode.Impulse); 
        m_BrakeScript.RemovePart();
        Destroy(gameObject, 2f);
    }
}
