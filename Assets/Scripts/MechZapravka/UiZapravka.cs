﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UiZapravka : MonoBehaviour
{
    [SerializeField] private CanvasGroup CanvasGroupEngine;
    [SerializeField] private Image NeededIcon;
    [SerializeField] private Sprite[] NeededSpritesAll;
    [SerializeField] private Material[] NewFillMaterialsCar;
    [SerializeField] private Material[] NewFillMaterialParticle;
    [SerializeField] private ParticleSystemRenderer[] AllParticle;
    [SerializeField] private GameObject[] ButtonColor;
    [SerializeField] private Sprite[] SpriteColor;
    private int CurrenId;

    void Start()
    {
        SetColorImage();
    }
    private void SetColorImage()
    {
        for (int i = 0; i < ButtonColor.Length; i++)
        {
            ButtonColor[i].GetComponent<Image>().sprite = SpriteColor[i];
        }
        int random = Random.Range(0, ButtonColor.Length);
        int currenLevelEngine = CarScript.Instance.GetNumberEngine;
        NeededIcon.sprite = NeededSpritesAll[random];
    }
    private void ActiveCanvas(int count, bool active)
    {
        CanvasGroupEngine.DOFade(count, 0.5f);
        CanvasGroupEngine.blocksRaycasts = active;

    }
    public void CurrentCullinder(int id)
    {
        ActiveCanvas(0, false);
        for (int i = 0; i < AllParticle.Length; i++)
        {
            AllParticle[i].material = NewFillMaterialParticle[id];
        }
        ZapravkaController.Instance.ActiveFillColor(NewFillMaterialsCar[id]);
    }
    void Update()
    {
        
    }
}
