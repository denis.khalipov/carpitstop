﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class ZapravkaController : Singleton<ZapravkaController>
{
    Renderer RendShader;
    float FillAmount = 0;
    float FillCurrentLevel;
    [SerializeField] private GameObject Pictolet;
    [SerializeField] private ParticleSystem EffectFill;
    [SerializeField] private GameObject[] BackGround;
    [SerializeField] private GameObject BakOpen;
    [SerializeField] private Bak AllBak; // пока один , в планах несколько
    [SerializeField] private Image FailImage;
    [SerializeField] private Image Tutorial;
    
    Vector3 StartPosPistol;
    Vector3 StartRotatePistol;
    Sequence MoveSequence;
    private CarScript CurrentCar;
    bool ActiveFill = false;
    bool Fail = false;
    bool finish = false;
    bool SetFill = false;
    void Start()
    {
        RendShader = Wobble.Instance.GetRendMaterial;
        StartPosPistol = Pictolet.transform.localPosition;
        StartRotatePistol = Pictolet.transform.localEulerAngles;
        CurrentCar = CarScript.Instance;
        BackGround[CurrentCar.GetNumberBak].SetActive(true);
        BackGround[CurrentCar.GetNumberBak].GetComponent<MeshRenderer>().material = CurrentCar.GetMaterialCar;

        FillAmount = 0.81f; // Random.Range(0.55f, 0.8f);
        FillCurrentLevel = FillAmount;
        RendShader.material.SetFloat("_FillAmount", FillAmount);

        int i = PlayerPrefs.GetInt("Tutor_2", 0);
        if (i == 0)
        {
            Tutorial.gameObject.SetActive(true);
            PlayerPrefs.SetInt("Tutor_2", 1);
        }
    }

    public void ActiveFillColor(Material FillMaterial)
    {
        AllBak.gameObject.GetComponent<MeshRenderer>().material = FillMaterial;
        SetFill = true;
    }
    void Update()
    {
        if (!finish && SetFill)
        {
            if (Input.GetMouseButtonDown(0))
            {

                Tutorial.gameObject.SetActive(false);
                MoveSequence = DOTween.Sequence();
                MoveSequence.Append(Pictolet.transform.DOLocalRotate(new Vector3(-60, 175, -2), 0.4f)).OnComplete(() => { ActiveFill = true; EffectFill.Play(); });
                MoveSequence.Join(Pictolet.transform.DOLocalMove(new Vector3(-1.396f, -0.867f, -0.599f), 0.4f));
                MoveSequence.Join(BakOpen.transform.DOLocalRotate(new Vector3(0, 150, 0), 0.4f));

            }
            if (Input.GetMouseButton(0))
            {
                if (ActiveFill)
                {
                    VibrationController.Instance.VibrateWithTypeSelection();
                    FillAmount += -Time.deltaTime / 14;
                    RendShader.material.SetFloat("_FillAmount", FillAmount);
                    if (FillAmount < 0.25f && !Fail)
                    {
                        Fail = true;
                        StartCoroutine(FailGame());
                    }
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                MoveSequence.Kill();
                ActiveFill = false;
                EffectFill.Stop();
                Pictolet.transform.DOLocalMove(StartPosPistol, 0.4f);
                Pictolet.transform.DOLocalRotate(StartRotatePistol, 0.4f);
                if (!Fail)
                {
                    BakOpen.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.4f);
                }
                if (FillAmount > 0.27f && FillAmount < 0.333f)
                {
                    CurrentCar.WinNextMech();
                    finish = true;
                }
            }
        }
    }
    private IEnumerator FailGame()
    {
        AllBak.ActiveLoseEffecet();
        yield return new WaitForSeconds(1.3f);
         FailImage.DOFade(1, 0.3f);
        yield return new WaitForSeconds(2f);
        GameController.Instance.FadeNextMechanis();
        yield return new WaitForSeconds(0.45f);
        RendShader.material.SetFloat("_FillAmount", FillCurrentLevel);
        FillAmount = FillCurrentLevel;
        Fail = false;
        AllBak.ActiveLoseEffecet();
        FailImage.DOFade(0, 0);
        BakOpen.transform.DOLocalRotate(new Vector3(0, 0, 0), 0f);
    }
}
