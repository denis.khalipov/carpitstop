﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBalgarka : MonoBehaviour
{
    [SerializeField] private LayerMask mask;
    [SerializeField] private GameObject RotateObject;
    [SerializeField] private float Range;
    [SerializeField] private bool Balonchik = false;
    [SerializeField] private ParticleSystem EffectSpray;
    [SerializeField] private ParticleSystem EffectBalgarka;
    [SerializeField] private MeshRenderer ColorBalonchik;
    private bool m_IsInputActive = false;
    private Vector3 m_StartMovePosition;
    private Vector3 LastPos;
    private Vector3 StartPos;
    private void Start()
    {
        MaskDoor.Instance.SetObjectGrab(gameObject);
        StartPos = transform.position;
        if (Balonchik)
        {
            EffectSpray.startColor = CarScript.Instance.GetMaterialCar.color;
            ColorBalonchik.material = CarScript.Instance.GetMaterialCar;
        }
    }
    void Update()
    {
    }
    public void ActiveEffect(bool active)
    {
        if (Balonchik)
        {
            if (active)
            {
                EffectSpray.loop = true;
                EffectSpray.Play();
            }
            else
            {
                EffectSpray.loop = false;
            }
        }
        else
        {
            if (active)
            {
                EffectBalgarka.loop = true;
                EffectBalgarka.Play();
            }
            else
            {
                EffectBalgarka.loop = false;
            }
        }
    }
    public void MoveStart()
    {
        m_StartMovePosition = transform.position;
    }
    public void Move(Vector3 _value)
    {
        bool isCanMoove = false;

     

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, mask))
        {
            if (RotateObject)
            {
                RotateObject.transform.Rotate(0,0 , 35);
            }

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
            Debug.DrawRay(hit.point, hit.normal * hit.distance, Color.red);
            Vector3 dir = hit.normal;
            if (!Balonchik)
            {
                transform.up = Vector3.Lerp(transform.up, dir, Time.deltaTime * 5);
            }
            transform.position = hit.point + transform.TransformDirection(Vector3.up).normalized / Range;
            LastPos = transform.position;
            isCanMoove = true;
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, StartPos, Time.deltaTime * 10);
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 1000, Color.white);
            Debug.Log("Did not Hit");

        }
        if (isCanMoove)
        {
            transform.position = Vector3.Lerp(transform.position, m_StartMovePosition + _value, Time.deltaTime * 4);
        }
        else
        {
            transform.position = LastPos;
            MaskDoor.Instance.InputToWorldPoint(true);
        }
    }
}
