﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class DoorController : Singleton<DoorController>
{
    [SerializeField] private Canvas CurrentCanvas;
    [SerializeField] private Button DoneButton;
    [SerializeField] private Image Bar;
    [SerializeField] private ParticleSystem EffectWin;
    public enum Stay
    {
        Game,
        NextGrab,
        Finish,
    }
    public Stay StayGame = Stay.Game;
    [SerializeField] private GameObject[] CarsLevels;
    [SerializeField] private GameObject[] GrabObjects;
    [SerializeField] private GameObject TargetPos;

    void Start()
    {
        DoneButton.onClick.AddListener(ClickButton);
        Instantiate(CarsLevels[CarScript.Instance.GetNumberCar], transform);
    }
    public void ActiveWinEffect()
    {
        EffectWin.Play();
    }
    public void ChecBar(float barCount)
    {
        Bar.fillAmount = barCount;
        if (barCount > 0.55)
        {
            DoneButton.interactable = true;
        }
        else
        {
            DoneButton.interactable = false;
        }
    }
    private void ClickButton()
    {
        MaskDoor.Instance.NextRound();
    }
    public void NextGrab()
    {
        ChecBar(0);
        StayGame = Stay.NextGrab;
        GrabObjects[0].transform.DOMove(TargetPos.transform.position, 0.5f).OnComplete(() =>
        {
            GrabObjects[0].SetActive(false);
            Vector3 Startpos = GrabObjects[1].transform.position;
            GrabObjects[1].transform.position = TargetPos.transform.position;
            GrabObjects[1].SetActive(true);
            GrabObjects[1].transform.DOMove(Startpos, 0.5f).OnComplete(()=> {
                StayGame = Stay.Game;
            });
        });




    }
}
