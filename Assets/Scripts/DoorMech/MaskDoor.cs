﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MaskDoor : Singleton<MaskDoor>
{
    public float FadeCount = 5;
    public float radius;
    private Texture2D texture;
    private Texture2D CurrenTextures,NextTexture;
    private Texture2D CurrenMask;
    private RaycastHit hitInfo;
    private float CountCurrenPixel;
    private float AllPixel;
    private Material MaskMaterial,BackMaskMaterial;
    [SerializeField] private int RandomPaint = 5;
    [SerializeField] private LayerMask MaskDoorLayer, InputMask;
    private MeshRenderer MeshDoor;
    private MeshRenderer[] AllPaintObject;
    private GameObject CurrentObjectGrab;
    private Material MaterialCar;
    private bool Win = false;
    private Vector3 MousePosCurrent;
    private Vector3 Dir;
    private Vector3 m_DeltaPosition = Vector3.zero;
    private Vector3 m_StartTouchPosition;
    private bool ActiveNextTexture = false;
    

    public void SetObjectGrab(GameObject obj)
    {
        CurrentObjectGrab = obj;
    }
    private void Start()
    {
        SetDoorTextureAndMesh();
        InstansMaterials();
        SetTextureUpdate();
    }
    private void SetDoorTextureAndMesh()
    {
        CurrenTextures = DoorStartController.Instance.CurrentTexture;
        NextTexture = DoorStartController.Instance.NextTexture;
        CurrenMask = DoorStartController.Instance.Mask;
        string nameDoor = DoorStartController.Instance.GetDoor.name;
        MeshDoor = GameObject.Find(nameDoor).GetComponent<MeshRenderer>();
        MeshDoor.gameObject.layer = 15;
        if (!MeshDoor.GetComponent<MeshCollider>())
        {
            MeshDoor.gameObject.AddComponent<MeshCollider>();
        }
    }
    private void SetTextureUpdate()
    {
        FadeCount = 30;
        radius = 8;
        AutoSettingMaterila();

        ChecAllPixel();
        MeshDoor.material.SetTexture("_AlphaMap", CurrenMask);
        MeshDoor.material.SetTexture("_MainTex", CurrenTextures);
        texture = new Texture2D(CurrenMask.width, CurrenMask.height);

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                texture.SetPixel(x, y, CurrenMask.GetPixel(x, y));
            }
        }
        texture.Apply();
    }
    private void InstansMaterials()
    {
        MaskMaterial = MaskMaterialController.Instance.GetMaskMaterial;
        BackMaskMaterial = MaskMaterialController.Instance.GetBackMaskMaterial;
        AddObjectPainting();
        for (int i = 0; i < AllPaintObject.Length; i++)
        {
            AllPaintObject[i].material = CarScript.Instance.GetMaterialCar;
        }
        MaterialCar = CarScript.Instance.GetMaterialCar;
    }
    private void AddObjectPainting()
    {
        MeshRenderer[] AllObjectPainting = CarScript.Instance.GetPaintingDetail;
        List<MeshRenderer> FindObjectAll = new List<MeshRenderer>();
        for (int i = 0; i < AllObjectPainting.Length; i++)
        {
            string NameObject = AllObjectPainting[i].gameObject.name;
            FindObjectAll.Add(GameObject.Find(NameObject).GetComponent<MeshRenderer>());
        }
        AllPaintObject = FindObjectAll.ToArray();

    }
    private void AutoSettingMaterila()
    {
        if (!ActiveNextTexture)
        {
            Material[] StartMaterial = new Material[2];
            StartMaterial[0] = MaskMaterial;
            StartMaterial[1] = BackMaskMaterial;
            BackMaskMaterial.mainTexture = NextTexture;
            MeshDoor.materials = StartMaterial;
        }
    }

    public void InputToWorldPoint(bool _isFirest)
    {
        RaycastHit hit;
        Ray ray;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 150, InputMask))
        {
            if (_isFirest)
            {
                m_StartTouchPosition = hit.point;
                CurrentObjectGrab.GetComponent<DoorBalgarka>().MoveStart();
            }
            else
            {
                m_DeltaPosition = hit.point - m_StartTouchPosition;
                CurrentObjectGrab.GetComponent<DoorBalgarka>().Move(m_DeltaPosition * 1.75f);

            }
        }
    }

    void Update()
    {

        if (DoorController.Instance.StayGame != DoorController.Stay.Game)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            InputToWorldPoint(true);
        }

        if (Input.GetMouseButton(0))
        {

            DoorController.Instance.ChecBar(CountCurrenPixel / AllPixel);
            InputToWorldPoint(false);
            CurrentObjectGrab.GetComponent<DoorBalgarka>().ActiveEffect(true);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(CurrentObjectGrab.transform.position, CurrentObjectGrab.transform.TransformDirection(Vector3.down), out hitInfo, Mathf.Infinity, MaskDoorLayer))
            {
                   Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hitInfo.distance, Color.yellow);
                UpdateTexture();
                if (!ActiveNextTexture)
                {
                  //  VibrationController.Instance.VibrateWithTypeSelection();
                }
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            CurrentObjectGrab.GetComponent<DoorBalgarka>().ActiveEffect(false);
            m_DeltaPosition = Vector3.Lerp(m_DeltaPosition, Vector3.zero, Time.deltaTime * 2);
            //Resources.UnloadUnusedAssets();
            if (AllPixel * 0.9f < CountCurrenPixel && !Win)
            {
                //  ControllerLevels.Instance.ChecWin();
                NextRound();
            }
        }
    }
    public void NextRound()
    {
        if (!ActiveNextTexture)
        {

            ActiveNextTexture = true;
            DoorController.Instance.NextGrab();
            NextTextureActive();
           
        }
        else if(!Win && ActiveNextTexture)
        {
            Win = true;
            CarScript.Instance.WinNextMech();
            DoorController.Instance.ActiveWinEffect();
        }
    }
    private void ChecAllPixel()
    {
        Color32[] NewPixel = CurrenMask.GetPixels32();
        Debug.Log(NewPixel);
        for (int i = 0; i < NewPixel.Length; i++)
        {
            if (NewPixel[i].r > 0 && NewPixel[i].g > 0 && NewPixel[i].b > 0)
            {
                AllPixel++;
            }

        }
    }
    private void NextTextureActive()
    {
        CurrenTextures = NextTexture;
        Material[] NewMaterial = MeshDoor.materials;
        NewMaterial[1] = MaterialCar;
        MeshDoor.materials = NewMaterial;
        AllPixel = 0;
        CountCurrenPixel = 0;
        SetTextureUpdate();
         FadeCount = 9;
        radius = 12;

    }
    public Texture2D CopyTexture2D(Texture2D copiedTexture2D)
    {
        float differenceX;
        float differenceY;

        texture.filterMode = FilterMode.Bilinear;
        texture.wrapMode = TextureWrapMode.Clamp;

        //Center of hit point circle 
        int m1 = (int)((hitInfo.point.x - hitInfo.collider.bounds.min.x) * (copiedTexture2D.width / hitInfo.collider.bounds.size.x));
        int m2 = (int)((hitInfo.point.y - hitInfo.collider.bounds.min.y) * (copiedTexture2D.height / hitInfo.collider.bounds.size.y));

        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                if (texture.GetPixel(x, y).r == 0 && texture.GetPixel(x, y).g == 0 && texture.GetPixel(x, y).b == 0)
                    continue;


                differenceX = x - m1;
                differenceY = y - m2;


                if (differenceX * differenceX + differenceY * differenceY <= radius * radius)
                {
                    //if (Random.Range(0, 99) > RandomPaint)
                    //{
                    //    continue;
                    //}
                    if (texture.GetPixel(x, y).r != 0 && texture.GetPixel(x, y).g != 0 && texture.GetPixel(x, y).b != 0)
                    {
                        Color32 ColorNew = Color32.Lerp(texture.GetPixel(x, y), new Color32(0, 0, 0, 0), Time.deltaTime * FadeCount);

                        if (texture.GetPixel(x, y).r < 0.3f && texture.GetPixel(x, y).g < 0.3f && texture.GetPixel(x, y).b < 0.3f)
                        {
                            ColorNew = Color32.Lerp(texture.GetPixel(x, y), new Color32(0, 0, 0, 0), Time.deltaTime * FadeCount * 100);
                        }
                        texture.SetPixel(x, y, ColorNew);
                    }
                    if (texture.GetPixel(x, y).r == 0 && texture.GetPixel(x, y).g == 0 && texture.GetPixel(x, y).b == 0)
                    {
                        CountCurrenPixel++;
                    }


                }
                else
                {
                    texture.SetPixel(x, y, copiedTexture2D.GetPixel(x, y));
                }
            }
        }

        texture.Apply();
        return texture;
    }

    public void UpdateTexture()
    {
        Texture2D newTexture2D = CopyTexture2D(texture);
        MeshDoor.material.SetTexture("_AlphaMap", newTexture2D);


    }
}
