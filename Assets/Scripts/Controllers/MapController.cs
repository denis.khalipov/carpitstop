﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MapController : MonoBehaviour
{
    [SerializeField] private GameObject[] AllMap;
    [SerializeField] private Image Fade;
    [SerializeField] private ParticleSystem Effect;
    private void Start()
    {
        for (int i = 0; i < AllMap.Length; i++)
        {
            AllMap[i].SetActive(false);
        }

        int numberGarade = PlayerPrefs.GetInt("LevelGarageOpen", 0);
        AllMap[numberGarade].SetActive(true);
    }
    public void ActiveMap()
    {
       
        Fade.DOFade(1, 0.5f).OnComplete(() =>
        {
            Effect.Play();
            for (int i = 0; i < AllMap.Length; i++)
            {
                AllMap[i].SetActive(false);
            }
            int numberGarade = PlayerPrefs.GetInt("LevelGarageOpen", 0);
            AllMap[numberGarade].SetActive(true);
            Fade.DOFade(0, 0.5f);
        });
      
    }
}
