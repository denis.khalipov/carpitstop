﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
public class StartUIController : Singleton<StartUIController>
{
    [SerializeField] private Button PlayButton, m_RewardPlay, SkipCarButton;
    [SerializeField] private Button NextFinishButton, GetBonusMechButton, m_NextRewardButton;
    [SerializeField] private Text CountSkipText;
    [SerializeField] private CanvasGroup StartContainer;
    [SerializeField] private Text CurrentGoldText,RepairCarGoldText;
    [SerializeField] private Button ButtunBuyGanage;
    [SerializeField] private GameObject[] BackGroundBuyGarage;
    [SerializeField] private Text TextCostBuyGarage;
    [SerializeField] private Button TapToPlay;
    [SerializeField] private CanvasGroup FaceContainer, FinishContainer, GetMechContainer;
    [SerializeField] private Sprite[] SpriteIconMech;
    [SerializeField] private Image GetMechImage;
    [SerializeField] private GameObject Sun;
    [SerializeField] private Image m_RewardIcon;

    private Sequence m_NextButtonSequence;

    private int CurrentGold;
    private int RepairSetGold;
    private int CountSkipCar;
    private int CostNewGarage;
    public bool NewGarageActive = false;
    public int GetRepairGold => RepairSetGold;

    //test
    [SerializeField] private Button NextGarage;
    [SerializeField] private Button DeletePlayerPref;
    [SerializeField] private Slider FadeSlide;
    [SerializeField] private CanvasGroup FadeObect;

    private int CurrenGarege = 0;
    void Start()
    {
        CurrentGold = PlayerPrefs.GetInt("Gold", 0);
        CurrentGoldText.text =CurrentGold.ToString();
        ButtunBuyGanage.onClick.AddListener(BuyNewGarage);
        PlayButton.onClick.AddListener(StartGame);
        m_RewardPlay.onClick.AddListener(RewardStartButtonClick);
        SkipCarButton.onClick.AddListener(SkipCar);
        NextFinishButton.onClick.AddListener(NextCar);
        m_NextRewardButton.onClick.AddListener(RewardNextCar);
        GetBonusMechButton.onClick.AddListener(GetNewMEch);
        TapToPlay.onClick.AddListener(TapToPlayClick);

        NextGarage.onClick.AddListener(NextgarageClick);
        DeletePlayerPref.onClick.AddListener(deleteall);

        ChecCountSkip();
        ChecActiveButtonGarage();
    }
    private void NextgarageClick()
    {
        int LevelGarage =  PlayerPrefs.GetInt("LevelGarageOpen", 0);      

        if (LevelGarage > 5)
        {
            LevelGarage = 0;
        }
        else
        {
            LevelGarage++;
        }
        PlayerPrefs.SetInt("LevelGarageOpen", LevelGarage);
        GetComponent<MapController>().ActiveMap();
    }
    private void deleteall()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }
    private void Update()
    {
        Sun.transform.Rotate(0, 0, 80 * Time.deltaTime);
        FadeObect.alpha = FadeSlide.value;
    }
    private void TapToPlayClick()
    {
        CarScript.Instance.MoveCarStartPos();
        GamePlayMenu.Instance.GameStart(); //fade стартового меню
    }
    public void ActiveBackGroundButtonGarage(bool active)
    {
        BackGroundBuyGarage[1].SetActive(active);
        BackGroundBuyGarage[0].SetActive(!active);
        ButtunBuyGanage.interactable = active;
        NewGarageActive = active;
    }
    public void ActiveFaceContainer(int fade)
    {
        FaceContainer.DOFade(fade, 0.4f);
    }
    public void SetRepairGoldCar(int Cost)
    {
        RepairSetGold = Cost;
        RepairCarGoldText.text = RepairSetGold.ToString() + "$";
    }
    private void BuyNewGarage()
    {
        GameController.Instance.OpenNewGarage();
    //    CurrentGold -= CostNewGarage;
        PlayerPrefs.SetInt("Gold", CurrentGold);
        GetComponent<MapController>().ActiveMap();
        ChecActiveButtonGarage();
    }
    private void StartGame()
    {
        GameController.Instance.StartGame();
        FadeStartButton(0,false);
    }

    private void RewardStartButtonClick()
    {
        if (AppLovinManager.Instance.IsRewardVideoReady)
        {
            AppLovinManager.Instance.ShowRewardedAd();
            AppLovinManager.Instance.Action_RewardPlayer += RewardStartGame;
        }
    }

    private void RewardStartGame(bool _isRewarded)
    {
        AppLovinManager.Instance.Action_RewardPlayer -= RewardStartGame;
        if (_isRewarded)
        {
            StartGame();
        }
    }

    public void FadeStartButton(int i , bool Ray)
    {
        StartContainer.blocksRaycasts = Ray;
        StartContainer.DOFade(i, 0.4f);
    }
    private void ChecActiveButtonGarage()
    {
        if (m_NextButtonSequence != null)
            m_NextButtonSequence.Kill();
        NextFinishButton.gameObject.SetActive(false);

        int CurrentGarageActive = PlayerPrefs.GetInt("LevelGarageOpen", 0);
        ScribleObjecCar[] AllGarage = GameController.Instance.GetAllGarage;
        if (CurrentGarageActive + 1 < AllGarage.Length)
        {
            CostNewGarage = AllGarage[CurrentGarageActive + 1].GetCostBuyGarag;
            TextCostBuyGarage.text = CostNewGarage.ToString();
            CurrentGoldText.text = CurrentGold.ToString();
        }
        else
        {
            TextCostBuyGarage.text = "MAX";
            CostNewGarage = 1000000000;
            CurrentGoldText.text = CurrentGold.ToString();
            //  ButtunBuyGanage.gameObject.SetActive(false);
        }
        if (CurrentGold >= CostNewGarage)
        {
            ActiveBackGroundButtonGarage(true);
        }
        else
        {
            ActiveBackGroundButtonGarage(false);
        }
    }
    private void ActiveGarageButton()
    {

    }
    private void SkipCar()
    {
        if (CountSkipCar < 3)
        {
            GameController.Instance.SkipCar();
            CountSkipCar++;
            ChecCountSkip();
            StartContainer.blocksRaycasts = false;
            StartContainer.DOFade(0, 0.5f);
        }
        else
        {
            if (AppLovinManager.Instance.IsRewardVideoReady)
            {
                AppLovinManager.Instance.ShowRewardedAd();
                AppLovinManager.Instance.Action_RewardPlayer += SkipCarWithReward;
            }
        }
    }

    private void SkipCarWithReward(bool _isRewarded)
    {
        AppLovinManager.Instance.Action_RewardPlayer -= SkipCarWithReward;
        if (_isRewarded)
        {
            GameController.Instance.SkipCar();
            StartContainer.blocksRaycasts = false;
            StartContainer.DOFade(0, 0.5f);
        }
    }

    private void ChecCountSkip()
    {
        CountSkipText.text = CountSkipCar + "/"+ "3";
        if (CountSkipCar >= 3)
        {
            m_RewardIcon.gameObject.SetActive(true);
            CountSkipText.gameObject.SetActive(false);
        }
    }

    private void NextCar()
    {
        ChecGold();
        ChecActiveButtonGarage();
        FinishContainer.blocksRaycasts = false;
        FinishContainer.DOFade(0, 1).OnComplete(() => {
            CountSkipCar = 0;
            ChecCountSkip();
            LevelController.Instance.ChecOnNextOpenMech();
            GameController.Instance.ActiveWinEffect();
        });    
    }

    private void RewardNextCar()
    {
        if (AppLovinManager.Instance.IsRewardVideoReady)
        {
            AppLovinManager.Instance.Action_RewardPlayer += RewardPlayer;
            AppLovinManager.Instance.ShowRewardedAd();
        }
    }

    private void RewardPlayer(bool _isRewarded)
    {
        AppLovinManager.Instance.Action_RewardPlayer -= RewardPlayer;
        if (!_isRewarded)
        {
            NextCar();
        }
        else
        {
            //даём монеты х2
            CurrentGold += RepairSetGold * 2;
            PlayerPrefs.SetInt("Gold", CurrentGold);
            CurrentGoldText.text = CurrentGold.ToString();

            ChecActiveButtonGarage();
            FinishContainer.blocksRaycasts = false;
            FinishContainer.DOFade(0, 1).OnComplete(() =>
            {
                CountSkipCar = 0;
                ChecCountSkip();
                LevelController.Instance.ChecOnNextOpenMech();
                GameController.Instance.ActiveWinEffect();
            });
        }
    }

    private void ChecGold()
    {
        CurrentGold += RepairSetGold;
        PlayerPrefs.SetInt("Gold", CurrentGold);
        CurrentGoldText.text = CurrentGold.ToString();
    }
    public void ActiveFinishNextButton()
    {
        m_NextButtonSequence = DOTween.Sequence()
            .AppendInterval(2f)
            .OnComplete(() => NextFinishButton.gameObject.SetActive(true));

        int CurrenMechanicOpen = PlayerPrefs.GetInt("OpenMech", 1);
        if (CurrenMechanicOpen < 7)
            GetMechImage.sprite = SpriteIconMech[LevelController.Instance.GetAllMechanic[CurrenMechanicOpen]];
        FinishContainer.DOFade(1, 0);
        FinishContainer.blocksRaycasts = true;
    }
    public void ActiveBonusButton()
    {
        GetMechContainer.DOFade(1, 0);
        GetMechContainer.blocksRaycasts = true;
    }
    private void GetNewMEch()
    {
        BonusCar.Instance.GetNewMechEffect();
        GetMechContainer.blocksRaycasts = false;

        Image Fade = GameController.Instance.GetFade;
        GetMechContainer.DOFade(0, 1).OnComplete(()=> {
            Fade.DOFade(1, 0.8f).OnComplete(() =>
            {
                BonusCar.Instance.DestoyAllObject();
                Fade.DOFade(0, 0.8f);
            });
        });      
    }

    public void SwitchUI(bool _isBonusCar)
    {
        PlayButton.gameObject.SetActive(!_isBonusCar);
        m_RewardPlay.gameObject.SetActive(_isBonusCar);
    }
}
