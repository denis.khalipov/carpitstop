﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelController : Singleton<LevelController>
{
    [SerializeField] private int[] SeqaunceMechanic;
    [SerializeField] private int[] OpenNewMech;
    private int CurrenMechanicOpen;
    private int CurrentLevel = 0;
    public int[] GetAllMechanic => SeqaunceMechanic;
    public int[] GetOpenNewMech => OpenNewMech;
    public bool BonusCarActiveMesh;

    void Start()
    {
        
     
    }
    public int[] GetOpenRandomMech()
    {
        List<int> OpenMesh = new List<int>();
        CurrenMechanicOpen = PlayerPrefs.GetInt("OpenMech", 1);
        for (int i = 0; i < CurrenMechanicOpen; i++)
        {
            OpenMesh.Add(SeqaunceMechanic[i]);
        }
        if (CurrenMechanicOpen > 3)
        {
            OpenMesh.Remove(Random.Range(0, 2));
        }
        List<int> RandomMech = OpenMesh.OrderBy(mech => System.Guid.NewGuid()).Take(OpenMesh.Count >= 3 ? 3 : OpenMesh.Count).ToList(); 
        int[] Number = RandomMech.ToArray();
        for (int i = 0; i < Number.Length; i++)
        {
            Debug.Log(Number[i]);
        }
     

        return RandomMech.Take(Random.Range(1, RandomMech.Count + 1)).ToArray(); // Number;  
    }
    public void ChecOnNextOpenMech()
    {
        CurrentLevel = PlayerPrefs.GetInt("Level", 0);
        if (CurrenMechanicOpen < SeqaunceMechanic.Length && CurrentLevel == OpenNewMech[CurrenMechanicOpen - 1] - 1)
        {

            Debug.Log("Открытие новой механики");
            CurrenMechanicOpen++;
            PlayerPrefs.SetInt("OpenMech", CurrenMechanicOpen);
            BonusCarActiveMesh = true;
        }
        GameController.Instance.NextCar();
    }
}
