﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using System.Linq;
using Random = UnityEngine.Random;

public class GameController : Singleton<GameController>
{
    public event Action Action_StepComplete;
    public event Action Action_LevelComplete;
    public event Action Action_GameStart;
    [SerializeField] private GameObject BonusCar;
    [SerializeField] private GameObject BonusBox;
    [SerializeField] private Material WellMaterial;
    [SerializeField] private ScribleObjecCar[] AllLevelsGarageScribl;
    [SerializeField] private Material[] AllMaterialCar;
    [SerializeField] private GameObject[] AllCars;
    [SerializeField] private GameObject[] AllMech;
    [SerializeField] private Image Fade;
    [SerializeField] private GameObject StartMechanic;
    [SerializeField] private ParticleSystem WinEffect;
    private GameObject Car;
    private Vector3 offset;
    private int CurrentLevel;
    private int CurrentLevelGarage;
    int CurrentGarage;
    int RandomCar;
    int SpawnNewCarCount = 1;
    bool WinCarActive = false;
    Sequence mySequence;
    public GameObject GetBonusBox => BonusBox;
    public GameObject[] GetAllMech => AllMech;
    public ScribleObjecCar[] GetAllGarage => AllLevelsGarageScribl;
    public Material GetWellMaterial => WellMaterial;
    public Image GetFade => Fade;
    public GameObject SetCurrnetCar
    {
        set { Car = value; }
    }
    public Material GetCarMaterila
    {
        get { return AllMaterialCar[Random.Range(0, AllMaterialCar.Length)]; }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            NextCar();
        }
    }
    void Start()
    {
        Application.targetFrameRate = 60;
        CurrentLevel = PlayerPrefs.GetInt("Level", 0);
        CurrentLevelGarage = PlayerPrefs.GetInt("LevelGarageOpen", 0);
        CheckSpawnCar();
        Fade.DOFade(0, 1);
    }
    private void SpawnCar()
    {
        Car = Instantiate(AllLevelsGarageScribl[CurrentGarage].GetCars[RandomCar], StartMechanic.transform);
        StartUIController.Instance.SetRepairGoldCar(AllLevelsGarageScribl[CurrentGarage].GetCostRandom);
        Car.transform.parent = null;
        Car.transform.position -= transform.forward;
    }
    private void SpawnNewCar()
    {
        RandomCar = Random.Range(0, AllLevelsGarageScribl[CurrentGarage + 1].GetCars.Length);
        Car = Instantiate(AllLevelsGarageScribl[CurrentGarage + 1].GetCars[RandomCar], StartMechanic.transform);
        Car.GetComponent<CarScript>().NewCar = true;
        StartUIController.Instance.SetRepairGoldCar(AllLevelsGarageScribl[CurrentGarage + 1].GetCostRandom);
        Car.transform.parent = null;

        Car.transform.position -= transform.forward;
    }
    private void SpawnBonusCar()
    {
        var bonusCar = Instantiate(BonusCar, BonusCar.transform.position, BonusCar.transform.rotation);

    }
    private void ChecLevelCar()
    {
        int Chance = Random.Range(1, 101);
        int[] Garage = AllLevelsGarageScribl[CurrentLevelGarage].GetProcent; //из скриблобщест берём массив процентов
        int GarageIndex = 0; // номер гаража 
        int Count = 0;
        for (int i = 0; i < Garage.Length; i++)
        {
            Count += Garage[i];

            if (Count >= Chance)
            {
                CurrentGarage = GarageIndex;
                break;
            }
            GarageIndex++;
        }
        RandomCar = Random.Range(0, AllLevelsGarageScribl[CurrentGarage].GetCars.Length); //какие машинки в левеле
    }
    public void OpenNewGarage()
    {
        CurrentLevelGarage++;
        PlayerPrefs.SetInt("LevelGarageOpen", CurrentLevelGarage);
    }
    public void StartGame()
    {
        CarScript.Instance.TapToPlay();
        transform.DOMove(transform.position, 1.6f).OnComplete(() =>
        {
            StartMechanic.SetActive(false);
        });
    }
    public void LevelStepComplete()
    {
        Action_StepComplete?.Invoke();
    }
    public void FadeNextMechanis()
    {
        Fade.DOFade(1, 0.5f).OnComplete(() =>
        {
            Fade.DOFade(0, 0.5f);
        });
    }
    public void NextCar()
    {
        CarScript.Instance.FinishMove();
        WinCarActive = true;
    }
    public void SkipCar()
    {
        CarScript.Instance.FinishMove();
        WinCarActive = false;
    }
    public void ActiveWinEffect()
    {
        WinEffect.Play();
    }
    public void Restart()
    {
        StartMechanic.SetActive(true);
        StartUIController.Instance.ActiveFinishNextButton();
    }
    private void CheckSpawnCar()
    {
        if (StartUIController.Instance.NewGarageActive && SpawnNewCarCount >= 1 && CurrentLevelGarage + 1 < AllLevelsGarageScribl.Length)
        {
            PlayerPrefs.SetInt("Level", CurrentLevel);
            ChecLevelCar();
            SpawnNewCar(); //спавнит новую машинку с нового гаража
            SpawnNewCarCount = 0;
            StartUIController.Instance.SwitchUI(true);
        }
        else
        {
            PlayerPrefs.SetInt("Level", CurrentLevel);
            ChecLevelCar();
            SpawnCar();
            StartUIController.Instance.SwitchUI(false);
            if (WinCarActive == true)
                SpawnNewCarCount++;
        }
    }
    public IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(0.1f);

        if (LevelController.Instance.BonusCarActiveMesh)
        {
            SpawnBonusCar();
            LevelController.Instance.BonusCarActiveMesh = false;
        }
        else
        {
            CurrentLevel = WinCarActive == true ? CurrentLevel += 1 : CurrentLevel; //пропуск уровня 

            CheckSpawnCar();
            if (WinCarActive)
            {
                AppLovinManager.Instance.ShowInterstitialVideo();
                Action_LevelComplete?.Invoke();
            }
            else
            {
                CarScript.Instance.MoveCarStartPos();
            }
        }
    }

    private void CreateRandomCar()
    {
        int index = Random.Range(0, AllCars.Length);
        Car = Instantiate(AllCars[index], StartMechanic.transform);
        Car.transform.parent = null;

        int[] sequnMech = new int[Random.Range(2, 4)];
        List<int> mechIndex = new List<int>();

        for (int i = 1; i < AllMech.Length; i++)
        {
            mechIndex.Add(i);
        }
        mechIndex = mechIndex.OrderBy(mech => Guid.NewGuid()).ToList();
        for (int i = 0; i < sequnMech.Length; i++)
        {
            sequnMech[i] = mechIndex[i];
        }
        Car.GetComponent<CarScript>().SetSetSequenceMechanics = sequnMech;
    }
}
