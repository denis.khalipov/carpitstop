﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BonusCar : Singleton<BonusCar>
{
    [SerializeField] private GameObject Car;
    [SerializeField] private GameObject Box;
    [SerializeField] private ParticleSystem Effect;
    void Start()
    {
        MoveCarStartPos();
    }
    private void MoveCarStartPos()
    {
        //transform.DOMove(new Vector3(0, transform.position.y, -3), 1).OnComplete(() =>
        //{
        //    //for (int i = 0; i < SequenceMechanics.Length; i++)
        //    //{
        //    //    AllIcon[SequenceMechanics[i]].DOFade(1, 0.4f);
        //    //}
        //    //    GameController.Instance.TaptoPlayerActiveButton(); активация кнопок
        
        //});

        //for (int i = 0; i < Wells.Length; i++)
        //{
        //    Wells[i].transform.DOLocalRotate(new Vector3(5000, Wells[i].transform.localEulerAngles.y, Wells[i].transform.localEulerAngles.z), 1, RotateMode.FastBeyond360);
        //}
    }
    public void ActiveButton()
    {
        StartUIController.Instance.ActiveBonusButton();
    }
    public void GetNewMechEffect()
    {
        Box.GetComponent<MeshRenderer>().enabled = false;
        Effect.Play();
    }
    public void DestoyAllObject()
    {
        GameController.Instance.StartCoroutine(GameController.Instance.NextLevel());
        Destroy(gameObject);
    }

}
