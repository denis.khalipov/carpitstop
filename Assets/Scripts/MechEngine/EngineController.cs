﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EngineController : Singleton<EngineController>
{
    [SerializeField] private LayerMask BoltMask;
    [SerializeField] private Camera camera;
    [SerializeField] private GameObject[] BackGroundBody;
    [SerializeField] private Image Tutorial;
    [SerializeField] private GameObject[] Levels;
    Vector3 startPosMouse;
    Vector3 Dir;
    Transform objectHit;
    private List<GameObject> AllBolt = new List<GameObject>();
    private int CountBolt = 0;
    public GameObject SetBolt
    {
        set { AllBolt.Add(value); }
    }
    public List<GameObject> GetAllBolt
    {
        get { return AllBolt; }
    }
    void Start()
    {
        Levels[CarScript.Instance.GetNumberEngine].SetActive(true);
        int i = PlayerPrefs.GetInt("Tutor_1", 0);
        if(i == 0)
        {
            Tutorial.gameObject.SetActive(true);
            PlayerPrefs.SetInt("Tutor_1", 1);
        }
        BackGroundBody[CarScript.Instance.GetNumberEngine].GetComponent<MeshRenderer>().material = CarScript.Instance.GetMaterialCar;
    }
    void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            Tutorial.gameObject.SetActive(false);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, BoltMask))
            {
                objectHit = hit.transform;
                startPosMouse = camera.ScreenToViewportPoint(Input.mousePosition);
                Dir = objectHit.transform.position - camera.ScreenToViewportPoint(Input.mousePosition);
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (objectHit)
            {
                EngineBolt obj = objectHit.GetComponent<EngineBolt>();
                if(startPosMouse.y < camera.ScreenToViewportPoint(Input.mousePosition).y && !obj.NewBolt)
                {
                    objectHit.transform.position =  new Vector3(objectHit.transform.position.x,camera.ScreenToViewportPoint(Input.mousePosition).y + Dir.y,objectHit.transform.position.z);
                }
                else if(startPosMouse.y > camera.ScreenToViewportPoint(Input.mousePosition).y && !obj.BoltActive && obj.NewBolt)
                {
                    objectHit.GetComponent<EngineBolt>().ChecPosY();
                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, camera.ScreenToViewportPoint(Input.mousePosition).y + Dir.y, objectHit.transform.position.z);
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (objectHit )
            {
                objectHit.GetComponent<EngineBolt>().ChecPosY();
            }
            objectHit = null;

        }
    }
    public void ChecOffBolt()
    {
        CountBolt++;
        if (AllBolt.Count == CountBolt)
        {
            Win();
        }
    }
    private void Win()
    {
        CarScript.Instance.WinNextMech();
    }

}
