﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class EngineBolt : MonoBehaviour
{
    private Vector3 StartPos;
    public bool NewBolt = false;
    public bool BoltActive = false;
    [SerializeField] private Material NewMaterial;
    [SerializeField] private GameObject StartEffect, EndEffect;
    private MeshRenderer Mr;
    void Start()
    {
        StartPos = transform.position;
        EngineController.Instance.SetBolt = gameObject;
        Mr = GetComponent<MeshRenderer>();
        
    }
    public void ChecPosY()
    {
       
        if (!NewBolt)
        {
            if (transform.position.y > StartPos.y + 0.1f)
            {
                StartEffect.GetComponent<ParticleSystem>().loop = false;
                StartEffect.transform.GetChild(0).GetComponent<ParticleSystem>().loop = false;
                //StartEffect.SetActive(false);
                VibrationController.Instance.VibrateWithTypeMEDIUMIMPACT();
                transform.DOMoveY(StartPos.y + 3f, 0.5f).OnComplete(() =>
                {
                    NewBolt = true;
                    Mr.material = NewMaterial;
                    UiEngine.Instance.SetBolt = gameObject;
                });
            }
        }
        else
        {
            if (Vector3.Distance(new Vector3(0, transform.position.y, 0), new Vector3(0, StartPos.y, 0)) < 0.01f && !BoltActive || !BoltActive && transform.position.y <= StartPos.y) 
            {
                EndEffect.SetActive(true);
                VibrationController.Instance.VibrateWithTypeMEDIUMIMPACT();
                BoltActive = true;
                GetComponent<BoxCollider>().enabled = false;
                transform.DOMoveY(StartPos.y, 0.1f).OnComplete(() =>
                {
                    EngineController.Instance.ChecOffBolt();
                });
            }
        }
    }
    public void GoBackPos()
    {
        transform.DOMoveY(StartPos.y + 0.1f, 0.5f);
    }
   
}
