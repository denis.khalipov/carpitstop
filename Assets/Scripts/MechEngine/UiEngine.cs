﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UiEngine : Singleton<UiEngine>
{
    [SerializeField] private CanvasGroup CanvasGroupEngine;
    [SerializeField] private Image NeededIcon;
    [SerializeField] private Sprite[] NeededSpritesLevel_1, NeededSpritesLevel_2;
    [SerializeField] private Mesh[] NewCylinderMesh;
    [SerializeField] private Material[] NewCylinderMaterials;
    [SerializeField] private GameObject[] ButtonColor;
    [SerializeField] private Sprite[] SpriteColor;
    private List<GameObject> AllCylinderObject = new List<GameObject>();
    private int CurrenId;
    public GameObject SetBolt
    {
        set { AllCylinderObject.Add(value);
            if(AllCylinderObject.Count == EngineController.Instance.GetAllBolt.Count)
            {
                ActiveCanvas(1,true);
            }
        }
    }
    void Start()
    {
        SetColorImage();
    }
    private void SetColorImage()
    {
        for (int i = 0; i < ButtonColor.Length; i++)
        {
            ButtonColor[i].GetComponent<Image>().sprite = SpriteColor[i];
        }
        int random = Random.Range(0, ButtonColor.Length);
        int currenLevelEngine = CarScript.Instance.GetNumberEngine;
        NeededIcon.sprite = currenLevelEngine == 0 ? NeededSpritesLevel_1[random] : NeededSpritesLevel_2[random];
    }
    private void ActiveCanvas(int count ,bool active)
    {
        CanvasGroupEngine.DOFade(count, 0.5f);
        CanvasGroupEngine.blocksRaycasts = active;
    }
    public void CurrentCullinder(int id)
    {
        AllCylinderObject = EngineController.Instance.GetAllBolt;
        StartCoroutine(GoBackCylinders(id));
        ActiveCanvas(0, false);
    }
    private IEnumerator GoBackCylinders(int id)
    {
        for (int i = 0; i < AllCylinderObject.Count; i++)
        {
            yield return new WaitForSeconds(0.1f);
            AllCylinderObject[i].GetComponent<MeshFilter>().mesh = NewCylinderMesh[id];
            AllCylinderObject[i].GetComponent<MeshRenderer>().material = NewCylinderMaterials[id];
            AllCylinderObject[i].GetComponent<EngineBolt>().GoBackPos();
        }
       
    }
}
