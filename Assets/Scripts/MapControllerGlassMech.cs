﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControllerGlassMech : MonoBehaviour
{
    [SerializeField] private GameObject[] AllMap;

    private void Start()
    {
        for (int i = 0; i < AllMap.Length; i++)
        {
            AllMap[i].SetActive(false);
        }

        int numberGarade = PlayerPrefs.GetInt("LevelGarageOpen", 0);
        AllMap[numberGarade].SetActive(true);
    }
}
