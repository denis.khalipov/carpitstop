﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Cars", menuName = "New Levels Cars", order = 1)]
public class ScribleObjecCar : ScriptableObject
{
    [SerializeField] private GameObject[] Cars;
    [SerializeField] private int[] ProcentProgress;
    [SerializeField] private int[] Cost;
    [SerializeField] private int CostBuyGarage;
    public int GetCostBuyGarag
    {
        get { return CostBuyGarage; }
    }
    public int GetCostRandom
    {
        get { return Random.Range(Cost[0], Cost[1]); }
    }
    public int[] GetProcent
    {
        get { return ProcentProgress; }
    }
    public GameObject[] GetCars
    {
        get { return Cars; }
    }
}
