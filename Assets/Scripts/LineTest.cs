﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTest : MonoBehaviour
{

    [SerializeField] private LineRenderer Line;
    [SerializeField] private GameObject Finish;
    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        Line.SetPosition(0, transform.position);
        Line.SetPosition(1, Finish.transform.position);
    }
}
