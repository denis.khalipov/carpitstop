﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class AppLovinManager : Singleton<AppLovinManager>
{
    public event Action<bool> Action_RewardPlayer;

    private const string MaxSdkKey = "MGTguhciKMVhiznzWUgyag_hzN2Orb7CsgtlMURhsxNTv9VQeBNDtMwqlyDZTKmP0c8M6Z3yVRVPa0rEBCS2TM";
    private const string InterstitialAdUnitId = "1e0927bfb95d9e68";
    private const string RewardedAdUnitId = "ced88235e11181b4";
    private const string BannerAdUnitId = "eba473b3124f1d27";

    private bool isBannerShowing = false; 
    private bool m_IsCanShowADS = false;

    private float m_LastInterstitialTime = 0f;

    public bool ActivateADS
    {
        set
        {
            m_IsCanShowADS = value;
        }
    }

    public bool IsRewardVideoReady
    {
        get
        {
            return MaxSdk.IsRewardedAdReady(RewardedAdUnitId);
        }
    }

    void Start()
    {        
        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
        {
            //MaxSdk.ShowMediationDebugger();
            InitializeInterstitialAds();
            InitializeRewardedAds();
            InitializeBannerAds();
        };
        m_LastInterstitialTime = Time.time;
        MaxSdk.SetSdkKey(MaxSdkKey);
        MaxSdk.InitializeSdk();  
    }

    public void ShowInterstitialVideo()
    {
        if (Time.time - m_LastInterstitialTime > 35f)
        {
            m_LastInterstitialTime = Time.time;
            ShowInterstitial();
        }
    }

    public void ShowRewardedAd()
    {
        if (MaxSdk.IsRewardedAdReady(RewardedAdUnitId))
        {
            MaxSdk.ShowRewardedAd(RewardedAdUnitId);
        }
        else
        {
            //rewardedStatusText.text = "Ad not ready";
        }
    }


    #region Interstitial Ad Methods

    private void InitializeInterstitialAds()
    {
        // Attach callbacks
        MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;

        // Load the first interstitial
        LoadInterstitial();
    }

    void LoadInterstitial()
    {
        MaxSdk.LoadInterstitial(InterstitialAdUnitId);
    }

    void ShowInterstitial()
    {
        if (MaxSdk.IsInterstitialReady(InterstitialAdUnitId))
        {
            m_IsCanShowADS = false;
            Debug.Log("666 ad ready");
            MaxSdk.ShowInterstitial(InterstitialAdUnitId);
        }
        else
        {
            Debug.Log("666 ad not ready");
        }
    }

    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        Debug.Log("Interstitial loaded");
    }

    private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
    {
        Debug.Log("Interstitial failed to load with error code: " + errorCode);
        Invoke("LoadInterstitial", 3);
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        Debug.Log("Interstitial failed to display with error code: " + errorCode);
        LoadInterstitial();
    }

    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        LoadInterstitial();
    }

    #endregion

    #region Rewarded Ad Methods

    private void InitializeRewardedAds()
    {
        // Attach callbacks
        MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
        MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
        MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        // Load the first RewardedAd
        LoadRewardedAd();
    }

    private void LoadRewardedAd()
    {
        MaxSdk.LoadRewardedAd(RewardedAdUnitId);
    }    

    private void OnRewardedAdLoadedEvent(string adUnitId)
    {
        Debug.Log("Rewarded ad loaded");
    }

    private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
    {
        Debug.Log("Rewarded ad failed to load with error code: " + errorCode);
        Invoke("LoadRewardedAd", 3);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        Debug.Log("Rewarded ad failed to display with error code: " + errorCode);
        LoadRewardedAd();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId)
    {
        Debug.Log("Rewarded ad displayed");
    }

    private void OnRewardedAdClickedEvent(string adUnitId)
    {
        Debug.Log("Rewarded ad clicked");
    }

    private void OnRewardedAdDismissedEvent(string adUnitId)
    {
        Debug.Log("Rewarded ad dismissed");
        Action_RewardPlayer?.Invoke(false);
        LoadRewardedAd();
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
    {
        Debug.Log("Rewarded ad received reward");
        Action_RewardPlayer?.Invoke(true);
    }

    #endregion

    #region Banner Ad Methods

    private void InitializeBannerAds()
    {
        MaxSdk.CreateBanner(BannerAdUnitId, MaxSdkBase.BannerPosition.BottomCenter);
        MaxSdkCallbacks.OnBannerAdLoadedEvent += OnBannerLoaded;
    }

    private void OnBannerLoaded(string adUnitId)
    {
        MaxSdk.ShowBanner(BannerAdUnitId);
        isBannerShowing = true;
    }

    private void ToggleBannerVisibility()
    {
        if (!isBannerShowing)
        {
            MaxSdk.ShowBanner(BannerAdUnitId);
            isBannerShowing = true;
        }
        else
        {
            MaxSdk.HideBanner(BannerAdUnitId);
            isBannerShowing = false;
        }
    }

    #endregion
}