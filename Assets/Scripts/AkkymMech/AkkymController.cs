﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class AkkymController : Singleton<AkkymController>
{
    [SerializeField] private Camera camera;
    [SerializeField] private LayerMask BoltMask;
    private Vector3 mOffset;
    private float mZCoord;
    private Vector3 StartPosHitObject;
    Transform FerstKlemaGrab;
    private GameObject StartBolt;
    private List<GameObject> AllBolt = new List<GameObject>();
    private int CountActiveBolt;
    [SerializeField] private GameObject[] AllLevels;
    [SerializeField] private ParticleSystem EffectConnection;
    [SerializeField] private MeshRenderer MeshCar;
    public ParticleSystem GetEffectConnect
    {
        get { return EffectConnection; }
    }
    public GameObject AddBolt
    {
        set { AllBolt.Add(value); }
    }
    public void ChecWinGame()
    {
        CountActiveBolt++;
        if (CountActiveBolt >= AllBolt.Count)
        {
            CarScript.Instance.WinNextMech();
        }
    }
    void Update()
    {
        MoveObject();
    }
    private void Start()
    {
        MeshCar.material = CarScript.Instance.GetMaterialCar;
        int i = PlayerPrefs.GetInt("AkkymLevel", 0);
        Instantiate(AllLevels[i], transform);
        i++;
        if (i >= AllLevels.Length)
        {
            PlayerPrefs.SetInt("AkkymLevel", 0);
        }
        else
        {
           
            PlayerPrefs.SetInt("AkkymLevel", i);
        }
    }
    private void MoveObject()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, BoltMask))
            {
                if (hit.collider.GetComponent<BoltForKlema>().GetFirestKlem)
                {
                    StartBolt = hit.collider.gameObject;
                    GameObject Klema = hit.collider.GetComponent<BoltForKlema>().GetFirestKlem;
                    if (Klema.GetComponent<Klema>().Active)
                    {
                        FerstKlemaGrab = hit.collider.GetComponent<BoltForKlema>().GetFirestKlem.transform;
                        VibrationController.Instance.VibrateWithTypeSelection();
                        StartPosHitObject = FerstKlemaGrab.transform.position;
                        FerstKlemaGrab.GetComponent<Klema>().GrabActive();
                        mZCoord = Camera.main.WorldToScreenPoint(FerstKlemaGrab.transform.position).z;
                        mOffset = FerstKlemaGrab.transform.position - GetMouseAsWorldPoint();
                    }
                }
            }

        }
        if (Input.GetMouseButton(0) && FerstKlemaGrab)
        {
            //позиция  за мышкой
            FerstKlemaGrab.transform.position = GetMouseAsWorldPoint() + mOffset;
            FerstKlemaGrab.transform.position = new Vector3(FerstKlemaGrab.transform.position.x, -0.17f, FerstKlemaGrab.transform.position.z);
        }
        if (Input.GetMouseButtonUp(0) && FerstKlemaGrab)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.GetComponent<BoltForKlema>() &&
                    hit.collider.GetComponent<BoltForKlema>().Number != StartBolt.GetComponent<BoltForKlema>().Number)
                {
                    VibrationController.Instance.VibrateWithTypeSelection();
                    BoltForKlema NextBolt = hit.collider.GetComponent<BoltForKlema>();
                    FerstKlemaGrab.GetComponent<Klema>().MoveNewPos(NextBolt.gameObject.transform.localPosition, NextBolt.GetCountAllKlema);
                    NextBolt.AddObject = FerstKlemaGrab.gameObject;
                    StartBolt.GetComponent<BoltForKlema>().SetRemoveObject = FerstKlemaGrab.gameObject;
                    Clear();
                }
                else
                {
                    FerstKlemaGrab.GetComponent<Klema>().GrabOff();
                    Clear();
                }
            }

        }


    }
    private void Clear()
    {
        FerstKlemaGrab = null;
        StartBolt = null;
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        // Convert it to world point
        return Camera.main.ScreenToWorldPoint(mousePoint);

    }
}
