﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Klema : MonoBehaviour
{
    private Vector3 StartPOs;
    public bool Active = true;
    public int Number;
    
    public void GrabActive()
    {
        StartPOs = transform.position;
        GetComponent<BoxCollider>().enabled = false;
        Active = false;
    }
    public void GrabOff()
    {
        transform.DOMove(StartPOs, 0.4f).OnComplete(() =>
        {
            Active = true;
            GetComponent<BoxCollider>().enabled = true;
        });
    }
    public void MoveNewPos(Vector3 pos, int countKlem)
    {
        Vector3 newpos = new Vector3(pos.x, 0 + (countKlem * 0.03f), pos.z);
        transform.DOLocalMove(newpos, 0.4f).OnComplete(() =>
        {
            Active = true;
            GetComponent<BoxCollider>().enabled = true;
        });
    }
}
