﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    [SerializeField] private GameObject Pos_2;
    [SerializeField] private LineRenderer LineCurren;
    void Start()
    {
        LineCurren.SetPosition(0, transform.position);
    }
    void Update()
    {
        LineCurren.SetPosition(1, Pos_2.transform.position);

    }
}
