﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltForKlema : MonoBehaviour
{
    [SerializeField] private List<GameObject> AllKlema = new List<GameObject>();
    public int Number;
    private bool Finish = false;
    public bool DefauldBolt = false;
    private ParticleSystem EffectWin;
    private void Start()
    {
        if (!DefauldBolt && AllKlema.Count > 0 && AllKlema[0].GetComponent<Klema>().Number != Number || !DefauldBolt && AllKlema.Count == 0)
        {
            AkkymController.Instance.AddBolt = gameObject;
        }
        if(AllKlema.Count > 0)
        {
            for (int i = 0; i < AllKlema.Count; i++)
            {
                AllKlema[i].transform.position = new Vector3(transform.position.x, AllKlema[i].transform.position.y+ (i * 0.03f), transform.position.z);
            }
           if(AllKlema[0].GetComponent<Klema>().Number == Number && !Finish)
            {
                Finish = true;
                Debug.Log("Нашампурил");
            }
        }
        
    }
    public int GetCountAllKlema
    {
        get { return AllKlema.Count; }//добавление всех клем в начале
    }
    public GameObject GetFirestKlem
    {
        get
        {
            if (AllKlema.Count > 0 && !Finish || AllKlema.Count > 1 && Finish) 
            {

                return AllKlema[AllKlema.Count - 1];
            }
            else
            {
                return null;
            }
        }
    }
    public GameObject SetRemoveObject
    {
        set
        {
            AllKlema.Remove(value);
        }
    }
    public GameObject AddObject
    {
        set
        {
            AllKlema.Add(value);
            if (AllKlema[0].GetComponent<Klema>().Number == Number && !Finish)
            {
                Finish = true;
                Debug.Log("Нашампурил");
                AkkymController.Instance.ChecWinGame();
                EffectWin = Instantiate(AkkymController.Instance.GetEffectConnect, transform);

                EffectWin.Play();
            }
        }
    }
}
