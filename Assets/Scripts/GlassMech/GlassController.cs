﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GlassController : Singleton<GlassController>
{
    [SerializeField] private GameObject[] CarsLevels;
    [SerializeField] private ObjectGrab GrabObject;
    [SerializeField] private Canvas CurrentCanvas;
    [SerializeField] private Button DoneButton;
    [SerializeField] private Image Bar;
    [SerializeField] private ParticleSystem EffectWin;
    public Canvas GetCanvas => CurrentCanvas;
    void Start()
    {
        DoneButton.onClick.AddListener(ClickButton);
        Instantiate(CarsLevels[CarScript.Instance.GetNumberCar], transform);
        GrabObject.SetObject();
    }
    public void ChecBar(float barCount)
    {
        Bar.fillAmount = barCount;
        if(barCount > 0.55)
        {
            DoneButton.interactable = true;
        }
    }
    public void ActiveWinEffect()
    {
        EffectWin.Play();
    }
    private void ClickButton()
    {
        MaskScript.Instance.NextMech();
    }
}
