﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorStartController : Singleton<DoorStartController>
{
    [SerializeField] private MeshRenderer DoorPainting;
    public Texture2D CurrentTexture, NextTexture, Mask;
    public MeshRenderer GetDoor => DoorPainting;
}
