﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MaskScript : Singleton<MaskScript>
{
    public float FadeCount = 5;
    public float radius;
    private Texture2D texture;
    private Texture2D CurrenTextures;
    private Texture2D CurrenMask;
    private RaycastHit hitInfo;
    private float CountCurrenPixel;
    private float AllPixel;
    private GameObject ObjectGrab;
    private Camera camera;
    private MeshRenderer WindonwMech;
    [SerializeField] private LayerMask MaskGlass,InputMask;
    private MeshRenderer[] BodyAndDoors;
    private bool Win = false;
    private Vector3 m_DeltaPosition = Vector3.zero;
    private Vector3 m_StartTouchPosition;
  
    public GameObject SetObjectGrab
    {
        set { ObjectGrab = value; }
    }
    private void Start()
    {
        CurrenTextures = GlassStartController.Instance.GetTexture;
        CurrenMask = GlassStartController.Instance.GetMask;



        AddObjectPainting();
        for (int i = 0; i < BodyAndDoors.Length; i++)
        {
            BodyAndDoors[i].material = CarScript.Instance.GetMaterialCar;
        }
        camera = Camera.main;
        ChecAllPixel();
        SetAllInfoWindow();
       
    }
    private void SetAllInfoWindow()
    {
        Material[] NewMaterials = new Material[2];
        NewMaterials[0] = MaskMaterialController.Instance.GetMaskMaterial;
        NewMaterials[1] = MaskMaterialController.Instance.GetGlass;
        string nameWindow = GlassStartController.Instance.GetWindowMesh.name;
        WindonwMech = GameObject.Find(nameWindow).GetComponent<MeshRenderer>();
        WindonwMech.gameObject.layer = 13;
        WindonwMech.materials = NewMaterials;
        if (!WindonwMech.GetComponent<MeshCollider>())
        {
            WindonwMech.gameObject.AddComponent<MeshCollider>();
        }
        WindonwMech.material.SetTexture("_AlphaMap", CurrenMask);
        WindonwMech.material.SetTexture("_MainTex", CurrenTextures);
        texture = new Texture2D(CurrenMask.width, CurrenMask.height);

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                texture.SetPixel(x, y, CurrenMask.GetPixel(x, y));
            }
        }
        texture.Apply();
    }
   private void AddObjectPainting()
    {
        MeshRenderer[] AllObjectPainting = CarScript.Instance.GetPaintingDetail;
        List<MeshRenderer> FindObjectAll = new List<MeshRenderer>();
        for (int i = 0; i < AllObjectPainting.Length; i++)
        {
            string NameObject = AllObjectPainting[i].gameObject.name;
            FindObjectAll.Add(GameObject.Find(NameObject).GetComponent<MeshRenderer>());
        }
        BodyAndDoors = FindObjectAll.ToArray();

    }
    public void InputToWorldPoint(bool _isFirest)
    {
        RaycastHit hit;
        Ray ray;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 150, InputMask))
        {
            if (_isFirest)
            {
                m_StartTouchPosition = hit.point;
                ObjectGrab.GetComponent<ObjectGrab>().MoveStart();
            }
            else
            {
                m_DeltaPosition = hit.point - m_StartTouchPosition;
                ObjectGrab.GetComponent<ObjectGrab>().Move(m_DeltaPosition * 1.75f);
            }
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            InputToWorldPoint(true);
        }
        if (Input.GetMouseButton(0))
        {

            InputToWorldPoint(false);

            GlassController.Instance.ChecBar(CountCurrenPixel/AllPixel);
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ObjectGrab.transform.position, ObjectGrab.transform.TransformDirection(Vector3.down), out hitInfo, Mathf.Infinity, MaskGlass))
            {
                UpdateTexture();
               // VibrationController.Instance.VibrateWithTypeSelection();
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            m_DeltaPosition = Vector3.Lerp(m_DeltaPosition, Vector3.zero, Time.deltaTime * 2);
            //Resources.UnloadUnusedAssets();

            ObjectGrab.GetComponent<ObjectGrab>().DontMove();
            if (AllPixel * 0.9f < CountCurrenPixel && !Win)
            {
                NextMech();

            }
        }
    }
    public void NextMech()
    {
        if (!Win)
        {
            Win = true;
            CarScript.Instance.WinNextMech();
            GlassController.Instance.ActiveWinEffect();
        }
    }
    private void ChecAllPixel()
    {
        Color32[] NewPixel = CurrenMask.GetPixels32();
        Debug.Log(NewPixel);
        for (int i = 0; i < NewPixel.Length; i++)
        {
            if(NewPixel[i].r > 0 && NewPixel[i].g >0 && NewPixel[i].b > 0)
            {
                AllPixel++;
            }
           
        }
    }
    public Texture2D CopyTexture2D(Texture2D copiedTexture2D)
    {
        float differenceX;
        float differenceY;

        texture.filterMode = FilterMode.Bilinear;
        texture.wrapMode = TextureWrapMode.Clamp;

        //Center of hit point circle 
        int m1 = (int)((hitInfo.point.x - hitInfo.collider.bounds.min.x) * (copiedTexture2D.width / hitInfo.collider.bounds.size.x));
        int m2 = (int)((hitInfo.point.y - hitInfo.collider.bounds.min.y) * (copiedTexture2D.height / hitInfo.collider.bounds.size.y));
        
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                if (texture.GetPixel(x, y).r == 0 && texture.GetPixel(x, y).g == 0 && texture.GetPixel(x, y).b ==0)
                    continue;


                differenceX = x - m1;
                differenceY = y - m2;
                
                
                if (differenceX * differenceX + differenceY * differenceY <= radius*radius)
                {
                    //if (Random.Range(0, 99) > RandomPaint)
                    //{
                    //    continue;
                    //}
                    if (texture.GetPixel(x, y).r != 0&& texture.GetPixel(x, y).g != 0&& texture.GetPixel(x, y).b != 0)
                    {
                        Color32 ColorNew = Color32.Lerp(texture.GetPixel(x, y), new Color32(0, 0, 0, 0), Time.deltaTime * FadeCount);

                        if(texture.GetPixel(x, y).r <0.3f && texture.GetPixel(x, y).g < 0.3f && texture.GetPixel(x, y).b < 0.3f)
                        {
                            ColorNew = Color32.Lerp(texture.GetPixel(x, y), new Color32(0, 0, 0, 0), Time.deltaTime * FadeCount*100);
                        }
                        texture.SetPixel(x, y, ColorNew);
                    }
                    if (texture.GetPixel(x, y).r== 0&& texture.GetPixel(x, y).g == 0 && texture.GetPixel(x, y).b == 0)
                    {
                        CountCurrenPixel++;
                    }
                    
                    
                }
                else
                {
                    texture.SetPixel(x, y, copiedTexture2D.GetPixel(x, y));
                }
            }
        }
        
        texture.Apply();
        return texture;
    }

    public void UpdateTexture()
    {
        Texture2D newTexture2D = CopyTexture2D(texture);
        WindonwMech.material.SetTexture("_AlphaMap", newTexture2D);
      

    }
}
