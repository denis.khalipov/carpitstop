﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGrab : MonoBehaviour
{
    [SerializeField] private LayerMask mask;
    [SerializeField] private GameObject RotateObject;
    [SerializeField] private ParticleSystem[] EffectWater;
    private bool m_IsInputActive = false;
    private Vector3 m_StartMovePosition;
    private Vector3 LastPos;
    public float Speed = 500;
    private void Start()
    {
       
    }
    public void SetObject()
    {
        MaskScript.Instance.SetObjectGrab = gameObject;
    }
    void Update()
    {

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(transform.forward * Time.deltaTime /3);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(-transform.forward * Time.deltaTime / 3);
        }
       

    }
    public void MoveStart()
    {
        m_StartMovePosition = transform.position;
        for (int i = 0; i < EffectWater.Length; i++)
        {
            EffectWater[i].loop = true;
            EffectWater[i].Play();
        }
      
    }
    public void DontMove()
    {
        for (int i = 0; i < EffectWater.Length; i++)
        {
            EffectWater[i].loop = false;
        }
    }
    public void Move(Vector3 _value)
    {
        bool isCanMoove = false;
       
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, mask))
        {
          
            RotateObject.transform.Rotate(0, Speed*Time.deltaTime, 0);
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
            Debug.DrawRay(hit.point, hit.normal * hit.distance, Color.red);
            Vector3 dir = hit.normal;
          
            transform.up = Vector3.Lerp(transform.up, dir, Time.deltaTime * 5);
            transform.position = hit.point + transform.TransformDirection(Vector3.up).normalized / 80;
            LastPos = transform.position;
            isCanMoove = true;
        }
        else
        {
            transform.position = LastPos;
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);

        }
        if (isCanMoove)
        {
            transform.position = Vector3.Lerp(transform.position, m_StartMovePosition + _value, Time.deltaTime * 4);
            
        }
        else
        {
            transform.position = LastPos;
            MaskScript.Instance.InputToWorldPoint(true);
        }
    }
}
