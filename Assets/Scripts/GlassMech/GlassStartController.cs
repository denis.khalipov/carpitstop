﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassStartController : Singleton<GlassStartController>
{
    [SerializeField] private MeshRenderer WindowMesh;
    [SerializeField] private Texture2D Texture, Mask;
    public Texture2D GetTexture => Texture;
    public Texture2D GetMask => Mask;
    bool ActiveGlass = false;
    public MeshRenderer GetWindowMesh => WindowMesh;
    void Start()
    {

        int[] AllMech = CarScript.Instance.GetSequanceMechanics;
        for (int i = 0; i < AllMech.Length; i++)
        {
            if(AllMech[i] == 6)
            {
                Material NewMaterial = MaskMaterialController.Instance.GetMaskMaterial;
                WindowMesh.material = NewMaterial;
                WindowMesh.material.SetTexture("_AlphaMap", Mask);
                WindowMesh.material.SetTexture("_MainTex", Texture);
                ActiveGlass = true;
            }
        }
        if (!ActiveGlass)
        {
            GlassMaterialActive();
        }

    }
    public void GlassMaterialActive()
    {
        WindowMesh.material = MaskMaterialController.Instance.GetGlass;
    }
}
