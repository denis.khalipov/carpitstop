﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CarScript : Singleton<CarScript>
{
    [SerializeField] private Material CurrenMaterial;
    [SerializeField] private MeshRenderer[] PaintingDetails;
    [SerializeField] private GameObject FullCar;
    [SerializeField] private int NumberCar;
    [SerializeField] private GameObject[] Wells;
    [SerializeField] private SpriteRenderer[] AllIcon;
    [SerializeField] private int NumberWell;
    [SerializeField] private int NumberBak;
    [Range(0, 1)]
    [SerializeField] private int NumberEngine;
    private List<GameObject> CurrenMechanics = new List<GameObject>();
    [Range(0, 6)]
    [SerializeField] private int[] SequenceMechanics;
    private GameObject[] AllMehanics;
    private Material CarMaterial;
    private Material WellCarMaterial;
    private GameObject BoxBonus;
    int CurrentMech = 0;
    public bool NewCar = false;
    public int GetNumberBak => NumberBak;
    public int GetNumberWell => NumberWell;
    public Material GetMaterialCar => CarMaterial;
    public Material GetMaterialWell => WellCarMaterial;
    public int GetNumberCar => NumberCar;
    public int[] GetSequanceMechanics => SequenceMechanics;
    public int GetNumberEngine => NumberEngine;
    public int GetMechanicCount => SequenceMechanics.Length;
    public MeshRenderer[] GetPaintingDetail => PaintingDetails;
    public int[] SetSetSequenceMechanics
    {
        set
        {
            SequenceMechanics = value;
        }
    }

    void Start()
    {
        ChecNewBonusCar();
        SequenceMechanics = LevelController.Instance.GetOpenRandomMech();
        GamePlayMenu.Instance.SetUiData(SequenceMechanics.Length);

        Material NewColorCar = CurrenMaterial ==null ?   GameController.Instance.GetCarMaterila : CurrenMaterial;
        CarMaterial = NewColorCar;
        WellCarMaterial = GameController.Instance.GetWellMaterial;
        for (int i = 0; i < PaintingDetails.Length; i++)
        {
            PaintingDetails[i].material = null;
           PaintingDetails[i].material = NewColorCar;
        }
        AllMehanics = GameController.Instance.GetAllMech;
        for (int i = 0; i < AllMehanics.Length; i++)
        {
            var Mech = Instantiate(AllMehanics[i], AllMehanics[i].transform.position, AllMehanics[i].transform.rotation);
            CurrenMechanics.Add(Mech);
        }
       // MoveCarStartPos();
    }
    private void ChecNewBonusCar()
    {
        if (NewCar)
        {
            FullCar.SetActive(false);
            BoxBonus = Instantiate(GameController.Instance.GetBonusBox, transform);
        }
    }
    public void MoveCarStartPos()
    {

        transform.DOMove(new Vector3(0, transform.position.y, -3), 1).OnComplete(() =>
        {
            for (int i = 0; i < SequenceMechanics.Length; i++)
            {
                if (!NewCar)
                    AllIcon[SequenceMechanics[i]].DOFade(1, 0.4f);
            }

            StartUIController.Instance.FadeStartButton(1, true);
            StartUIController.Instance.ActiveFaceContainer(1);
        });

        for (int i = 0; i < Wells.Length; i++)
        {
            Wells[i].transform.DOLocalRotate(new Vector3(5000, Wells[i].transform.localEulerAngles.y, Wells[i].transform.localEulerAngles.z), 1, RotateMode.FastBeyond360);
        }
    }



    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            WinNextMech();
        }
    }

    public void TapToPlay()
    {
        StartCoroutine(Next());
    }

    public void WinNextMech()
    {
        GameController.Instance.LevelStepComplete();
        StartCoroutine(Next());
    }
    public void ActiveFullCar(bool _active) 
    {
        FullCar.SetActive(_active);
    }
    public void SetUpgradedWheel(GameObject _wheel)
    {
        for (int i = 0; i < Wells.Length; i++)
        {
            Transform oldWheel = Wells[i].transform;
            var wheel = Instantiate(_wheel, oldWheel.parent);
            wheel.transform.localScale = new Vector3(30, 30, 30);
            Vector3 newPos = oldWheel.localPosition;
            newPos.x = newPos.x + 0.1f * Mathf.Abs(newPos.x);
            wheel.transform.localPosition = newPos;
            wheel.transform.localEulerAngles = new Vector3(0, oldWheel.localPosition.x > 0 ? 180 : 0, 0);
            Destroy(oldWheel.gameObject);
            Wells[i] = wheel;
        }
    }

    private IEnumerator Next()
    {

        StartUIController.Instance.ActiveFaceContainer(0);
        yield return new WaitForSeconds(1f);
        GameController.Instance.FadeNextMechanis();
        yield return new WaitForSeconds(0.5f);
        ActiveFullCar(false);
        if (CurrentMech != 0)
        {
            CurrenMechanics[SequenceMechanics[CurrentMech - 1]].SetActive(false);
        }

        if (CurrentMech == SequenceMechanics.Length)
        {
            GameController.Instance.Restart();
            GetComponent<GlassStartController>().GlassMaterialActive();
            for (int i = 0; i < SequenceMechanics.Length; i++)
            {
                AllIcon[SequenceMechanics[i]].DOFade(0, 0f);
            }
            if (NewCar)
            {
                BoxBonus.SetActive(false);
            }
            FinishUI.Instance.InfoFinish(SequenceMechanics);
            ActiveFullCar(true);
        }
        else
        {
            CurrenMechanics[SequenceMechanics[CurrentMech]].SetActive(true);
            CurrentMech++;
        }
    }

    public void FinishMove()
    {

        StartUIController.Instance.ActiveFaceContainer(0);
        for (int i = 0; i < SequenceMechanics.Length; i++)
        {
            AllIcon[SequenceMechanics[i]].DOFade(0, 0.4f);
        }
        
        transform.DOMove(transform.position, 1).OnComplete(() => {
            for (int i = 0; i < Wells.Length; i++)
            {
                Wells[i].transform.DOLocalRotate(new Vector3(5000, Wells[i].transform.localEulerAngles.y, Wells[i].transform.localEulerAngles.z), 1, RotateMode.FastBeyond360);
            }

            transform.DOMove(transform.position + (transform.forward * 8), 1).OnComplete(() =>
            {
                for (int i = 0; i < CurrenMechanics.Count; i++)
                {
                    Destroy(CurrenMechanics[i]);
                }
                GameController.Instance.StartCoroutine(GameController.Instance.NextLevel());
                Destroy(gameObject);
            });
        });
      
    }
}
