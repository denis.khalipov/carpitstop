﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskMaterialController : Singleton<MaskMaterialController>
{
    [SerializeField] private Material MaskMaterial, BackMaskMaterial,Glass;
    public Material GetGlass => Glass;
    public Material GetMaskMaterial => MaskMaterial;
    public Material GetBackMaskMaterial => BackMaskMaterial;

}
